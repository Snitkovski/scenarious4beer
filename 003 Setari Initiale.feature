#language: en
#coding: utf-8

@tree

Feature: Setarile initiale

As Manager
I want Introducerea setarilor initiale
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Setarile initiale

	When I click command interface button "Companie"
	When I click command interface button "Company details"
	Then "BEER SRL (Companie)" window is opened
	And I move to the next attribute
	And I input "BEER SRL" text in "Legal name" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I input "J40/19999/201" text in "Numărul de înregistrare de stat" field
	And I move to the next attribute
	And I input "J40/1999/2016" text in "Numărul de înregistrare de stat" field
	And I move to the next attribute
	And I input "12.02.2016" text in "Data înregistrării" field
	And I move to the next attribute
	And I select from "Registration country" drop-down list by "ro" string
	And I move to the next attribute
	And I click Select button of "Calendar producție" field
	Then "calendare" window is opened
	And I select current line in "List" table
	Then "BEER SRL (Companie) *" window is opened
	And I click "Salvare" button
	And I click "Salvare" button
	And I move to "Adrese, numere de telefon" tab
	And I move to "Grupul pagini" tab
	And I move to "Informații suplimentare" tab
	And I move to "Grupul pagini" tab
	And I move to "Logo and print" tab
	And I move to "Grupul pagini" tab
	And I move to "Informații generale" tab
	And I move to "Grupul pagini" tab
	And In this window I click command interface button "Conturi bancare"
	And In this window I click command interface button "Documente legale"
	And In this window I click command interface button "Politica contabilă"
	And I go to line in "List" table
		| 'Metoda inventarului contabil' | 'Metoda numerică de contabilitate' | 'Companie' | 'Pornește de la' | 'Înregistrați pentru TVA' | 'Default VAT rate' |
		| 'CMP'                          | 'Nu'                               | 'BEER SRL' | '01.01.1980'     | 'Nu'                      | '19%'              |
	And In this window I click command interface button "Responsible persons"
	And In this window I click command interface button "Fișiere"
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	#  When I click command interface button "Companie"
	#  When I click command interface button "Setări"
	#  When I click command interface button "Companie"
	#  Then "Companie" window is opened
	And In the command interface I select "Setări" "Companie"
	Then "Companie" window is opened
	#And I change checkbox "Accounting by departments "
	#And I click "Business units catalog" button
	#Then "Departamente" window is opened
	#And I close "Departamente" window
	#Then "Companie" window is opened
	And I change checkbox "Plan company resource loading"
	And I click "Company resources catalog" button
	Then "Resursele companiei" window is opened
	And I click the button named "ListResourcesKindsCreate"
	Then "Tipul resursei întreprinderii (creare)" window is opened
	And I close "Tipul resursei întreprinderii (creare)" window
	Then "Resursele companiei" window is opened
	And I select current line in "ListResourcesKinds" table
	Then "All resources (Tipul resursei întreprinderii)" window is opened
	And I input "Resources" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And in the table "ResourcesList" I click the button named "ResourcesListCreate"
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Brasaj Vas 01" text in "Denumire" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I input "1 000" text in "Capacitatea" field
	#And I click Select button of "Capacitatea" field
	And I move to the next attribute
	And I click "Salvare" button
	And I click "Specify resource kinds" button
	Then "1C:Enterprise" window is opened
	And I click "OK" button
	Then "Company resource kinds" window is opened
	And I close "Company resource kinds" window
	Then "Brasaj Vas * (Company resource)" window is opened
	And In this window I click command interface button "Resource operation schedules"
	And In this window I click command interface button "Variance from resource operation schedules"
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And in the table "ResourcesList" I click the button named "ResourcesListCopy"
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Brasaj Vas 02" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I add a line in "ListResourcesKinds" table
	Then "Tipul resursei întreprinderii (creare)" window is opened
	And I close "Tipul resursei întreprinderii (creare)" window
	Then "Resursele companiei" window is opened
	And in the table "ResourcesList" I click the button named "ResourcesListCopy"
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Brasaj Vas 03" text in "Denumire" field
	And I click "Salvare şi închidere" button
	When I click command interface button "Companie"
	Then "Resursele companiei" window is opened
	When I click command interface button "Departamente"
	When I click command interface button "Companie"
	Then "Departamente" window is opened
	When I click command interface button "Depozite"
	Then "Depozite" window is opened
	And I go to line in "List" table
		| 'Descriere'      | 'Tip'     | 'Slip' |
		| 'Vase de Brasaj' | 'Depozit' | 'Nu'   |
	And I select current line in "List" table
	Then "Vase de Brasaj (Unitate de afaceri a unei companii)" window is opened
	And I close "Vase de Brasaj (Unitate de afaceri a unei companii)" window
	Then "Depozite" window is opened
	And I move one level down in "List" table
	And I move one level up in "List" table
	And I move one level down in "List" table
	When I click command interface button "Resursele companiei"
	Then "Resursele companiei" window is opened
	And in the table "ResourcesList" I click the button named "ResourcesListCreate"
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Ferment Vas 01" text in "Denumire" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I input "505" text in "Capacitatea" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I click the button named "ListResourcesKindsCreate"
	Then "Tipul resursei întreprinderii (creare)" window is opened
	And I input "Vase de Brasaj" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I go to line in "ListResourcesKinds" table
		| 'Resource kind' |
		| 'Resources'     |
	And I go to line in "ResourcesList" table
		| 'Denumire'      |
		| 'Brasaj Vas 01' |
	And I select current line in "ResourcesList" table
	Then "Brasaj Vas * (Company resource)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I close "Brasaj Vas * (Company resource)" window
	Then "Resursele companiei" window is opened
	And I go to line in "ListResourcesKinds" table
		| 'Resource kind'  |
		| 'Vase de Brasaj' |
	And I select current line in "ListResourcesKinds" table
	Then "Vase de Brasaj (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Vase de Brasaj (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Vase de Brasaj (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Vase de Brasaj (Tipul resursei întreprinderii)" window is opened
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I click the button named "ListResourcesKindsCreate"
	Then "Tipul resursei întreprinderii (creare)" window is opened
	And I close "Tipul resursei întreprinderii (creare)" window
	Then "Resursele companiei" window is opened
	And I go to line in "ListResourcesKinds" table
		| 'Resource kind' |
		| 'Resources'     |
	And I go to line in "ResourcesList" table
		| 'Denumire'       |
		| 'Ferment Vas 01' |
	And I add a line in "ResourcesList" table
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Ferment Vas 02" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I add a line in "ResourcesList" table
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Ferment Vas 03" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I click the button named "ListResourcesKindsCreate"
	Then "Tipul resursei întreprinderii (creare)" window is opened
	And I input "Vase de Fermentatie" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I select current line in "ListResourcesKinds" table
	Then "Vase de Fermentatie (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Vase de Fermentatie (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Vase de Fermentatie (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I go to the last line in "List" table
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Vase de Fermentatie (Tipul resursei întreprinderii)" window is opened
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I go to line in "ListResourcesKinds" table
		| 'Resource kind'  |
		| 'Vase de Brasaj' |
	And I go to line in "ListResourcesKinds" table
		| 'Resource kind' |
		| 'Resources'     |
	And I click the button named "ListResourcesKindsCreate"
	Then "Tipul resursei întreprinderii (creare)" window is opened
	And I input "Tura de Productie 01" text in "Denumire" field
	And I input "Tura de Productie" text in "Denumire" field
	And I click "Salvare" button
	And in the table "ResourcesList" I click the button named "CreateResourceList"
	Then "Resource kind content (creare)" window is opened
	And I close "Resource kind content (creare)" window
	Then "Tura de Productie (Tipul resursei întreprinderii)" window is opened
	And I input "Ture de Productie" text in "Denumire" field
	And I click "Salvare" button
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I click the button named "FormCreate"
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Tura de Productie 01" text in "Denumire" field
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Ture de Productie (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I go to the last line in "List" table
	And I add a line in "List" table
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Tura de Productie 02" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Ture de Productie (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I go to the last line in "List" table
	And I add a line in "List" table
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Tura de Productie 03" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I select current line in "List" table
	Then "Resource kind content (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Ture de Productie (Tipul resursei întreprinderii)" window is opened
	And in the table "ResourcesList" I click "Creare" button
	Then "Resource kind content (creare)" window is opened
	And I click Select button of "Resursă" field
	Then "Resursele companiei" window is opened
	And I go to the last line in "List" table
	And I add a line in "List" table
	Then "Company resource (creare)" window is opened
	And in "TimetableSchedule" spreadsheet document I move to "R1C1" cell
	And I input "Tura de Productie 04" text in "Denumire" field
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I click the button named "FormChoose"
	Then "Resource kind content (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Ture de Productie (Tipul resursei întreprinderii)" window is opened
	And I click "Salvare şi închidere" button
	Then "Resursele companiei" window is opened
	And I go to line in "ListResourcesKinds" table
		| 'Resource kind' |
		| 'Resources'     |
	And In the command interface I select "Setări" "Companie"
	Then "Companie" window is opened
	And I click "States of events and jobs catalog" button
	Then "States of events and jobs" window is opened
	And I close "States of events and jobs" window
	Then "Companie" window is opened
	And I change checkbox "Include products and services SKU in print forms"
	And I change checkbox "Utilizați facturi fiscale"
	And I change checkbox "Use counterparty contract types"
	And I click "Set up company accounting" button
	Then "Set up company accounting" window is opened
	And I close "Set up company accounting" window
	When I click command interface button "Companie"
	Then "Companie" window is opened
	When I click command interface button "Company details"
	Then "BEER SRL (Companie)" window is opened
	And I select "Societate de Raspundere Limitata" exact value from "Forma juridică" drop-down list
	And I click "Salvare" button
	And I move to "Adrese, numere de telefon" tab
	And I move to "Grupul pagini" tab
	And I move to "Informații suplimentare" tab
	And I move to "Grupul pagini" tab
	And I move to "Logo and print" tab
	And I move to "Grupul pagini" tab
	And In this window I click command interface button "Conturi bancare"
	And In this window I click command interface button "Documente legale"
	And In this window I click command interface button "Politica contabilă"
	And I select current line in "List" table
	Then "Politica contabilă" window is opened
	And I change checkbox "Utilizați facturi fiscale"
	And I click "Salvare şi închidere" button
	Then "BEER SRL (Companie)" window is opened
	And In this window I click command interface button "Principală"
	And In this window I click command interface button "Principală"
	And I move to "Informații generale" tab
	And I move to "Grupul pagini" tab
	And I click "Salvare şi închidere" button
	#When I click command interface button "Setări"
	#Then "Companie" window is opened
	#When I click command interface button "Companie"
	#When I click command interface button "Setări"
	#Then "Companie" window is opened
	#When I click command interface button "Vânzări"
	#When I click command interface button "Setări"
	#Then "Vânzări" window is opened
	And In the command interface I select "Setări" "Achiziții"
	Then "Achiziții" window is opened
	#When I click command interface button "Achiziții"
	#Then "Achiziții" window is opened
	And I change checkbox "Contabilitatea prețurilor partenerului"
	#When I click command interface button "Setări"
	#Then "Achiziții" window is opened
	#When I click command interface button "Serviciu"
	#When I click command interface button "Setări"
	#Then "Serviciu" window is opened
	And In the command interface I select "Setări" "Producție"
	Then "Producție" window is opened
	#When I click command interface button "Producție"
	#Then "Producție" window is opened
	And I change checkbox "Permite utilizarea secțiunii"
	And I change checkbox "Utilizare operațiunile tehnologice"
	#When I click command interface button "Setări"
	#Then "Producție" window is opened
	And In the command interface I select "Setări" "Gestionarea numerarului"
	Then "Gestionarea numerarului" window is opened
	#When I click command interface button "Setări"
	#When I click command interface button "Gestionarea numerarului"
	#Then "Gestionarea numerarului" window is opened
	And I change "Calculation frequency" radio button value to "On period closing"
	#When I click command interface button "Setări"
	#Then "Gestionarea numerarului" window is opened
	When I click command interface button "Salarii"
	Then "Salarii" window is opened
	And I change checkbox "Permite utilizarea secțiunii"
	And I change checkbox "Keep staff list"
	And I change checkbox "Utilizare lucrul prin cumul"
	#When I click command interface button "Setări"
	#Then "Salarii" window is opened

