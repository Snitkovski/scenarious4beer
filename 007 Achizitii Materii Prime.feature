#language: en
#coding: utf-8

@tree

Feature: Achizitionarea materii prime

As Manager
I want Achizitionarea materii prime
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Achizitionarea materii prime

	When I click command interface button "Achiziții"
	When I click command interface button "Cumpărări mărfuri și servicii"
	Then "Cumpărări mărfuri și servicii" window is opened
	And I click the button named "FormCreate"
	Then "Factură furnizor (creare)" window is opened
	#And I click Select button of "datat" field
	And I input "04.06.2018 00:00:00" text in "datat" field
	And I move to the next attribute
	And I click Select button of "Furnizor" field
	Then "Furnizori" window is opened
	And I go to line in "List" table
		| 'Sumarul șirului de căutare'                             | 'Prezentare'                 | 'Nume legal'                 |
		| 'Furnizor Materii Prime 001\nCUI RO35431\nAdministrator' | 'Furnizor Materii Prime 001' | 'Furnizor Materii Prime 001' |
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click "PricesAndCurrency" hyperlink
	Then "Prețuri și valute" window is opened
	And I click "OK" button
	Then "Factură furnizor (creare) *" window is opened
	And in the table "Inventory" I click the button named "InventoryAdd"
	And I click choice button of "Produs sau serviciu" attribute in "Inventory" table
	Then "Produse și servicii" window is opened
	And I move one level down in "List" table
	And I move one level up in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "Seminte de grâu" exact value from the drop-down list named "InventoryProductsAndServices" in "Inventory" table
	And I move to the next attribute
	#And I click choice button of "Caracteristică" attribute in "Inventory" table
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I select current line in "List" table
	#Then "Factură furnizor (creare) *" window is opened
	#And I select "CLASA 1" exact value from "Caracteristică" drop-down list in "Inventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I click choice button of "Lot" attribute in "Inventory" table
	Then "Loturi" window is opened
	And I add a line in "List" table
	Then "Lotul de Produse și Servicii (creare)" window is opened
	And I input "04.06.2018" text in "Descriere" field
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Loturi" window is opened
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "04.06.2018" exact value from "Lot" drop-down list in "Inventory" table
	And I move to the next attribute
	And I input "1 000,000" text in "Cantitatea" field of "Inventory" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from the drop-down list named "InventoryMeasurementUnit" in "Inventory" table
	And I move to the next attribute
	And I input "3,33" text in "Prețul" field of "Inventory" table
	And I finish line editing in "Inventory" table
	#And I add a line in "Inventory" table
	#And I activate field named "InventoryLineNumber" in "Inventory" table
	#And I activate field named "InventoryProductsAndServices" in "Inventory" table
	#And I click choice button of "Produs sau serviciu" attribute in "Inventory" table
	#Then "Produse și servicii" window is opened
	#And I go to the last line in "List" table
	#And I move one line up in "List" table
	#And I select current line in "List" table
	#Then "Factură furnizor (creare) *" window is opened
	#And I select "Seminte de secară" exact value from the drop-down list named "InventoryProductsAndServices" in "Inventory" table
	#And I move to the next attribute
	#And I click choice button of "Caracteristică" attribute in "Inventory" table
	#Then "Caracteristicile nomenclatorului" window is opened
	#And in the table "List" I click the button named "ListCreate"
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I close "Caracteristică de produse și servicii (creare)" window
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I close "Caracteristicile nomenclatorului" window
	Then "Factură furnizor (creare) *" window is opened
	And I add a line in "Inventory" table
	And I activate field named "InventoryLineNumber" in "Inventory" table
	And I activate field named "InventoryProductsAndServices" in "Inventory" table
	And I click choice button of "Produs sau serviciu" attribute in "Inventory" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "DROJDII" exact value from the drop-down list named "InventoryProductsAndServices" in "Inventory" table
	And I move to the next attribute
	#And I click choice button of "Caracteristică" attribute in "Inventory" table
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I select current line in "List" table
	#Then "Factură furnizor (creare) *" window is opened
	#And I select "SORT 1" exact value from "Caracteristică" drop-down list in "Inventory" table
	#And I move to the next attribute
	And I click choice button of "Lot" attribute in "Inventory" table
	Then "Loturi" window is opened
	And I add a line in "List" table
	Then "Lotul de Produse și Servicii (creare)" window is opened
	And I input "04.06.2018" text in "Descriere" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Loturi" window is opened
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "04.06.2018" exact value from "Lot" drop-down list in "Inventory" table
	And I move to the next attribute
	And I input "1 000,000" text in "Cantitatea" field of "Inventory" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from the drop-down list named "InventoryMeasurementUnit" in "Inventory" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from the drop-down list named "InventoryMeasurementUnit" in "Inventory" table
	And I input "50,000" text in "Cantitatea" field of "Inventory" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from the drop-down list named "InventoryMeasurementUnit" in "Inventory" table
	And I move to the next attribute
	And I input "13,50" text in "Prețul" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I add a line in "Inventory" table
	And I activate field named "InventoryLineNumber" in "Inventory" table
	And I activate field named "InventoryProductsAndServices" in "Inventory" table
	And I click choice button of "Produs sau serviciu" attribute in "Inventory" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "HAMEI" exact value from the drop-down list named "InventoryProductsAndServices" in "Inventory" table
	And I move to the next attribute
	#And I click choice button of "Caracteristică" attribute in "Inventory" table
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I select current line in "List" table
	#Then "Factură furnizor (creare) *" window is opened
	#And I select "1 SORT" exact value from "Caracteristică" drop-down list in "Inventory" table
	#And I move to the next attribute
	And I click choice button of "Lot" attribute in "Inventory" table
	Then "Loturi" window is opened
	And I add a line in "List" table
	Then "Lotul de Produse și Servicii (creare)" window is opened
	And I input "04.06.2018" text in "Descriere" field
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Loturi" window is opened
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "04.06.2018" exact value from "Lot" drop-down list in "Inventory" table
	And I move to the next attribute
	And I input "100,000" text in "Cantitatea" field of "Inventory" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from the drop-down list named "InventoryMeasurementUnit" in "Inventory" table
	And I move to the next attribute
	And I input "55,50" text in "Prețul" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I add a line in "Inventory" table
	And I activate field named "InventoryLineNumber" in "Inventory" table
	And I activate field named "InventoryProductsAndServices" in "Inventory" table
	And I move one line up in "Inventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "Inventory" table
	And I input "7,50" text in "Prețul" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I move to "Servicii" tab
	And I move to "Pagini" tab
	And I move to "Avans" tab
	And I move to "Pagini" tab
	And I move to "Informații suplimentare" tab
	And I move to "Pagini" tab
	And I move to "Avans" tab
	And I move to "Pagini" tab
	And I move to "Servicii" tab
	And I move to "Pagini" tab
	And I move to "Bunuri" tab
	And I move to "Pagini" tab
	And I move to "Servicii" tab
	And I move to "Pagini" tab
	And I change checkbox "Include services in the inventory cost"
	And in the table "Expenses" I click the button named "ExpensesAdd"
	And I click choice button of "Produs sau serviciu" attribute in "Expenses" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I move one level down in "List" table
	And I click the button named "FormCreate"
	Then "Produse și servicii (creare)" window is opened
	And I input "Transport" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I change "Tip" radio button value to "Serviciu"
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I select current line in "List" table
	Then "Produse și servicii (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "Transport" exact value from the drop-down list named "ExpensesProductsAndServices" in "Expenses" table
	And I move to the next attribute
	And I move to the next attribute
	And I select "buc (unitate de stocare)" exact value from the drop-down list named "CostsMeasurementUnit" in "Expenses" table
	And I move to the next attribute
	And I input "1 234,00" text in "Prețul" field of "Expenses" table
	And I move to the next attribute
	And I finish line editing in "Expenses" table
	And I add a line in "Expenses" table
	And I activate field named "ExpensesLineNumber" in "Expenses" table
	And I activate field named "ExpensesProductsAndServices" in "Expenses" table
	And I move to "Bunuri" tab
	And I move to "Pagini" tab
	And I click "Validare şi închidere" button
	And in the table "Inventory" I click "conform sumei" button
	And I click "Validare şi închidere" button
	Then "Cumpărări mărfuri și servicii" window is opened
	And I click the button named "FormCreate"
	Then "Factură furnizor (creare)" window is opened
	And I click Select button of "Furnizor" field
	Then "Furnizori" window is opened
	And I add a line in "List" table
	Then "Partener (creare)" window is opened
	And I input "APA CANAL" text in "Nume legal" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select "Societate cu Raspundere Limitata" exact value from "Legal form" drop-down list
	And I move to the next attribute
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I input "RO987423" text in "CUI" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select from "Registration country" drop-down list by "R" string
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Detalii de contact" tab
	And I move to "GroupPages" tab
	And I move to "Domeniul de aplicare al activităților" tab
	And I move to "GroupPages" tab
	And I move to "Detalii de contact" tab
	And I move to "GroupPages" tab
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I move to "General" tab
	And I move to "GroupPages" tab
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Furnizori" window is opened
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	#And I click Select button of "datat" field
	And I input "12.06.2018  0:00:00" text in "datat" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And in the table "Inventory" I click the button named "InventoryAdd"
	And I select "APA" by string from the drop-down list named "InventoryProductsAndServices" in "Inventory" table
	And I move to the next attribute
	And I click choice button of "Caracteristică" attribute in "Inventory" table
	Then "Caracteristicile nomenclatorului" window is opened
	And I close "Caracteristicile nomenclatorului" window
	Then "Factură furnizor (creare) *" window is opened
	And I click choice button of "Produs sau serviciu" attribute in "Inventory" table
	And I click Open button of the field named "InventoryProductsAndServices"
	Then "APA (Produse și servicii)" window is opened
	#And I move to "Parametrii de stocare și cumpărare" tab
	#And I close "APA (Produse și servicii)" window
	#Then "APA (Produse și servicii)" window is opened
	And I move to "Parametrii de stocare și cumpărare" tab
	And I change checkbox "Caracteristici"
	And I click "Salvare" button
	And In this window I click command interface button "Caracteristici"
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "APA1" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "APA (Produse și servicii)" window is opened
	And I add a line in "List" table
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "APA2" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "APA (Produse și servicii)" window is opened
	And I move one line up in "List" table
	And I select current line in "List" table
	Then "APA1 (Caracteristică de produse și servicii)" window is opened
	And I close "APA1 (Caracteristică de produse și servicii)" window
	Then "APA (Produse și servicii)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Factură furnizor (creare) *" window is opened
	#And I select current line in "List" table
	#Then "Factură furnizor (creare) *" window is opened
	And I move to the next attribute
	And I click choice button of "Caracteristică" attribute in "Inventory" table
	Then "Caracteristicile nomenclatorului" window is opened
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I select "APA1" exact value from "Caracteristică" drop-down list in "Inventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I input "3 000,000" text in "Cantitatea" field of "Inventory" table
	And I move to the next attribute
	And I select "l (unitate de stocare)" exact value from the drop-down list named "InventoryMeasurementUnit" in "Inventory" table
	And I move to the next attribute
	And I input "0,13" text in "Prețul" field of "Inventory" table
	And I finish line editing in "Inventory" table
	#And I add a line in "Inventory" table
	#And I activate field named "InventoryLineNumber" in "Inventory" table
	#And I activate field named "InventoryProductsAndServices" in "Inventory" table
	#And I move to the next attribute
	#And I move to the next attribute
	#And I move to the next attribute
	#And I move to the next attribute
	#And I move to the next attribute
	#And I select current line in "Inventory" table
	#And I input "1,00" text in "Prețul" field of "Inventory" table
	#And I finish line editing in "Inventory" table
	#And I move to the next attribute
	#And I move to the next attribute
	#And I move to the next attribute
	#And I move to the next attribute
	#And I select current line in "Inventory" table
	#And I move to the next attribute
	And I click "Validare şi închidere" button
	#Then "Factură furnizor * de la *" window is opened
	#And I activate "Prețul" field in "Inventory" table
	#And I select current line in "Inventory" table
	#And I input "0,13" text in "Prețul" field of "Inventory" table
	#And I finish line editing in "Inventory" table
	#And I select current line in "Inventory" table
	#And I finish line editing in "Inventory" table
	#And I finish line editing in "Inventory" table
	#And I click "Validare şi închidere" button
	And I close "Cumpărări mărfuri și servicii" window

