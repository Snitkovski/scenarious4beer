﻿#language: en
#coding: utf-8

@tree

Feature: Completarea informațiilor si soldurilor initiale

As Manager
I want Completarea informațiilor si soldurilor initiale
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Completarea informațiilor si soldurilor initiale
 
	And I click the button named "FillInformationAboutCompany"
	Then "Completarea informațiilor despre companie (Pasul 1/5)" window is opened
	And I click "Înainte" button
	And I input "BEER SRL" text in "CompanyDescription" field
	And I click the button named "EnterAccountingPolicy"
	Then "Politica contabilă (creare)" window is opened
	And I change checkbox "Înregistrați pentru TVA"
	And I select "19%" exact value from "Default VAT rate" drop-down list
	And I click "Salvare şi închidere" button
	Then "Completarea informațiilor despre companie (Pasul 2/5) *" window is opened
	And I input "SRL" text in "CompanyDescriptionFull" field
	And I move to the next attribute
	And I click "Înainte" button
	And I input "RO1237890" text in "CUI" field
	And I click "Înainte" button
	And I input "Ion Popescu" text in "CEO full name" field
	And I move to the next attribute
	And I input "Diana Stanescu" text in "Numele complet al contabilului șef" field
	And I move to the next attribute
	And I input "Petru Stoian" text in "Cashier full name" field
	And I move to the next attribute
	And I input "Ilie Decan" text in "Warehouse supervisor full name" field
    And I click "Înainte" button
	And I click "Sfârșit" button
	And I click the button named "BalanceEntering"
	Then "Introducerea balanței de deschidere (Pasul0/5)" window is opened
	#And I click Select button of "Date of entering opening balance" field
	And I input "01.06.2018" text in "Date of entering opening balance" field
	And I move to the next attribute
	And I select "BEER SRL" exact value from "Companie" drop-down list
	And I change "SimpleModeAssistantUsage1" radio button value to "Advanced"
	And I click "Înainte" button
	And I select from "Moneda funcțională" drop-down list by "ro" string
	And I move to the next attribute
	And I select from "Moneda de prezentare" drop-down list by "ro" string
	And I move to the next attribute
	And in the table "EnterOpeningBalanceBankAndPettyCashCashAssets" I click the button named "EnterOpeningBalanceBankAndPettyCashCashAssetsAdd"
	#And I click choice button of "Bank (Cash) account" attribute in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	And I click choice button of "Bank (Cash) account" attribute in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	Then "Selectarea tipului de date" window is opened
	And I select current line in "" table
	#Then "Introducerea balanței de deschidere (Pasul1/5) *" window is opened
	#And I select "Cont bancar" exact value from "Bank (Cash) account" drop-down list in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	Then "Conturi bancare" window is opened
	And I click the button named "FormCreate"
	Then "Cont bancar (creare)" window is opened
	And I input "RO49BRDE123456789345" text in "Numărul contului de decontare" field
	And I move to the next attribute
	#And I select "RON" exact value from "Valută" drop-down list
	And I click Select button of "Valută" field
	Then "Valute" window is opened
	And I go to line in "Currencies" table
		| 'Nume valută' | 'Codul numeric' | 'Symbolic code' |
		| 'RON'         | '946'           | 'RON'           |
	And I select current line in "Currencies" table
	And I move to the next attribute
	And I click Choice button of the field named "SWIFTBank"
	Then "Bănci" window is opened
	And I add a line in "List" table
	Then "1C:Enterprise" window is opened
	And I click "Da" button
	Then "Bănci" window is opened
	And I click the button named "FormCreate"
	And I click the button named "FormChoose"
	And I click the button named "FormCreate"
	And I close "Bănci" window
	Then "Bănci" window is opened
	And I click the button named "FormChoose"
	And I close "Bănci" window
	Then "Cont bancar (creare) *" window is opened
	And I input "BRDEROBU" text in the field named "SWIFTBank"
	#And I click "Salvare şi închidere" button
	And I move to the next attribute
	Then "Banca nu a fost găsită" window is opened
	And I click "Selectați din catalog" button
	Then "Bănci" window is opened
	And I click the button named "FormChoose"
	And I click the button named "FormChoose"
	And I click the button named "FormChoose"
	And I click the button named "FormChoose"
	And I click the button named "FormCreate"
	Then "1C:Enterprise" window is opened
	And I click "Nu" button
	Then "Bancă (creare)" window is opened
	And I input "BRD Bucuresti" text in "Descriere" field
	And I move to the next attribute
	And I input "BRDEROBU" text in "BIC/SWIFT" field
	And I move to the next attribute
	And I input "sucursala Bratianu" text in "Branch" field
	And I click "Salvare şi închidere" button
	Then "Bănci" window is opened
	And I go to line in "List" table
		| 'Branch'             | 'BIC/SWIFT' | 'Descriere'     |
		| 'sucursala Bratianu' | 'BRDEROBU'  | 'BRD Bucuresti' |
	And I activate "BIC/SWIFT" field in "List" table
	And I select current line in "List" table
	Then "Cont bancar (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Conturi bancare" window is opened
	And I select current line in "List" table
	Then "Introducerea balanței de deschidere (Pasul1/5) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	#And I click choice button of "Sumă" attribute in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	And I input "1,00" text in "Sumă" field of "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	And I move to the next attribute
	#And I finish line editing in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	#And I add a line in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	#And I activate "Bank (Cash) account" field in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	#And I click choice button of "Bank (Cash) account" attribute in "EnterOpeningBalanceBankAndPettyCashCashAssets" table
	And I click "Înainte" button
	And I change "Utilizați mai multe depozite" radio button value to "Nu"
	And I change "Utilizați mai multe depozite" radio button value to "Da"
	And I change "You will use products and services characteristics" radio button value to "Da"
	And I change "You will use products and services batches" radio button value to "Da"
	And I input "DROJDII" text in "Produse și servicii" field
	And I click "Add products and services" button
	And I activate "UM" field in "EnterOpeningBalanceProductsInventory" table
	And I select current line in "EnterOpeningBalanceProductsInventory" table
	#And I click choice button of "UM" attribute in "EnterOpeningBalanceProductsInventory" table
	#And I activate "Produse și servicii" field in "EnterOpeningBalanceProductsInventory" table
	And I click Open button of the field named "EnterOpeningBalanceProductsInventoryProduct"
	#And I click Open button of "Produse și servicii" field
	Then "DROJDII (Produse și servicii)" window is opened
	#And I move to "Parametri principali" tab
	#And I move to "Parametrii de stocare și cumpărare" tab
	#And I move to "Pagini" tab
	#And I move to "Parametri principali" tab
	#And I move to "Pagini" tab
	#And I move to "Informații generale" tab
	#And I move to "Pagini" tab
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I go to line in "List" table
		| 'Short name' |
		| 'kg'         |
	And I select current line in "List" table
	Then "DROJDII (Produse și servicii) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Introducerea balanței de deschidere (Pasul2/5) *" window is opened
	And I finish line editing in "EnterOpeningBalanceProductsInventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "EnterOpeningBalanceProductsInventory" table
	And I select "kg (unitate de stocare)" exact value from "UM" drop-down list in "EnterOpeningBalanceProductsInventory" table
	And I finish line editing in "EnterOpeningBalanceProductsInventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "EnterOpeningBalanceProductsInventory" table
	And I input "10,000" text in "Cantitatea" field of "EnterOpeningBalanceProductsInventory" table
	And I finish line editing in "EnterOpeningBalanceProductsInventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "EnterOpeningBalanceProductsInventory" table
	And I input "10,00" text in "Prețul de achiziție" field of "EnterOpeningBalanceProductsInventory" table
	And I finish line editing in "EnterOpeningBalanceProductsInventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "EnterOpeningBalanceProductsInventory" table
	And I click choice button of "Business unit bin" attribute in "EnterOpeningBalanceProductsInventory" table
	Then "Depozite" window is opened
	And I move to the next attribute
	And I move one line up in "List" table
	And I select current line in "List" table
	Then "Introducerea balanței de deschidere (Pasul2/5) *" window is opened
	And I select "Depozitul Materia Prima" exact value from "Business unit bin" drop-down list in "EnterOpeningBalanceProductsInventory" table
	And I finish line editing in "EnterOpeningBalanceProductsInventory" table
	And I input "STICLA 0.5 L" text in "Produse și servicii" field
	And I click "Add products and services" button
	And I go to line in "EnterOpeningBalanceProductsInventory" table
		| 'Nr' | 'Business unit bin'   | 'UM'  | 'Cantitatea' | 'Produse și servicii' |
		| '2'  | 'Depozitul Principal' | 'buc' | '1,000'      | 'STICLA 0.5 L'        |
	And I activate "Cantitatea" field in "EnterOpeningBalanceProductsInventory" table
	And I select current line in "EnterOpeningBalanceProductsInventory" table
	And I input "1 000,000" text in "Cantitatea" field of "EnterOpeningBalanceProductsInventory" table
	And I finish line editing in "EnterOpeningBalanceProductsInventory" table
	And I move to the next attribute
	And I select current line in "EnterOpeningBalanceProductsInventory" table
	And I input "0,56" text in "Prețul de achiziție" field of "EnterOpeningBalanceProductsInventory" table
	And I finish line editing in "EnterOpeningBalanceProductsInventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click "Înainte" button
	And I click "Înainte" button
	And I click "Închis" button
	Then "1C:Enterprise" window is opened
	And I click "Da" button
	#Then "Introducerea balanței de deschidere (Pasul4/5)" window is opened
	#And I close "Introducerea balanței de deschidere (Pasul4/5)" window

