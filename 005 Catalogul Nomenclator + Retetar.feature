#language: en
#coding: utf-8

@tree

Feature: Indeplinirea catalogului Nomenclator si Retetar

As Manager
I want Indeplinirea catalogului Nomenclator si Retetar
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Indeplinirea catalogului Nomenclator si Retetar

	When I click command interface button "Vânzări"
	When I click command interface button "Produse și servicii"
	Then "Produse și servicii" window is opened
	And I click the button named "FormCreate"
	Then "Produse și servicii (creare)" window is opened
	And I input "APA" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Produse și servicii (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I click the button named "FormCreate"
	Then "Produse și servicii (creare)" window is opened
	And I input "Cereale de secară" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I go to line in "List" table
		| 'Short name' |
		| 'kg'         |
	And I select current line in "List" table
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I change checkbox "Caracteristici"
	And I change checkbox "Loturi"
	And I click Select button of "Depozit" field
	Then "Depozite, Departamente" window is opened
	And I go to line in "List" table
		| 'Descriere'               | 'Tip'     | 'Slip' |
		| 'Depozitul Materia Prima' | 'Depozit' | 'Nu'   |
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "Produse și servicii (creare) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere' | 'UM' | 'Tip'      |
		| '00-00000003' | 'APA'       | 'l'  | 'Inventar' |
	And I select current line in "List" table
	Then "APA (Produse și servicii)" window is opened
	And I move to "Parametri principali" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I click Select button of "Depozit" field
	Then "Depozite, Departamente" window is opened
	And I click the button named "FormCreate"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "TANK pt APA 01" text in "Descriere" field
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Depozite, Departamente" window is opened
	And I select current line in "List" table
	Then "APA (Produse și servicii) *" window is opened
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I add a line in "List" table
	Then "Produse și servicii (creare)" window is opened
	And I input "Seminte de grâu" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And I select "kg" exact value from "Unitate de masură" drop-down list
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'         | 'UM' | 'Tip'      |
		| '00-00000004' | 'Cereale de secară' | 'kg' | 'Inventar' |
	And I select current line in "List" table
	Then "Cereale de secară (Produse și servicii)" window is opened
	And I input "Seminte de secară" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'    | 'UM'  | 'Tip'      |
		| '00-00000002' | 'STICLA 0.5 L' | 'buc' | 'Inventar' |
	And I add a line in "List" table
	Then "Produse și servicii (creare)" window is opened
	And I input "BUTOI 60 L" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I change checkbox "Caracteristici"
	And I change checkbox "Loturi"
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And I select "l" exact value from "Unitate de masură" drop-down list
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	#And I go to the last line in "List" table
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'    | 'UM'  | 'Tip'      |
		| '00-00000002' | 'STICLA 0.5 L' | 'buc' | 'Inventar' |
	And I select current line in "List" table
	Then "STICLA 0.5 L (Produse și servicii)" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I change checkbox "Caracteristici"
	And I change checkbox "Loturi"
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	#And I go to line in "List" table
		#| 'Cod'         | 'Descriere' | 'UM' | 'Tip'      |
		#| '00-00000001' | 'DROJDII'   | 'kg' | 'Inventar' |
	#And I add a line in "List" table
	#Then "Produse și servicii (creare)" window is opened
	#And I close "Produse și servicii (creare)" window
	#Then "Produse și servicii" window is opened
	#And I select current line in "List" table
	#Then "DROJDII (Produse și servicii)" window is opened
	#And I move to "Parametrii de stocare și cumpărare" tab
	#And I close "DROJDII (Produse și servicii)" window
	#Then "Produse și servicii" window is opened
	#And I go to line in "List" table
		#| 'Cod'         | 'Descriere' | 'UM' | 'Tip'      |
		#| '00-00000003' | 'APA'       | 'l'  | 'Inventar' |
	#And I select current line in "List" table
	#Then "APA (Produse și servicii)" window is opened
	#And I move to "Parametrii de stocare și cumpărare" tab
	#And I close "APA (Produse și servicii)" window
	#Then "Produse și servicii" window is opened
	#And I go to line in "List" table
		#| 'Cod'         | 'Descriere'    | 'UM'  | 'Tip'      |
		#| '00-00000002' | 'STICLA 0.5 L' | 'buc' | 'Inventar' |
	#And I select current line in "List" table
	#And I go to line in "List" table
		#| 'Cod'         | 'Descriere'  | 'UM'  | 'Tip'      |
		#| '00-00000006' | 'BUTOI 60 L' | 'buc' | 'Inventar' |
	#And I go to line in "List" table
		#| 'Cod'         | 'Descriere' | 'UM' | 'Tip'      |
		#| '00-00000001' | 'DROJDII'   | 'kg' | 'Inventar' |
	#And I go to the last line in "List" table
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'    | 'UM'  | 'Tip'      |
		| '00-00000002' | 'STICLA 0.5 L' | 'buc' | 'Inventar' |
	And I select current line in "List" table
	Then "STICLA 0.5 L (Produse și servicii)" window is opened
	And I input "STICLA 0.5 L (euro)" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere' | 'UM' | 'Tip'      |
		| '00-00000001' | 'DROJDII'   | 'kg' | 'Inventar' |
	And I add a line in "List" table
	Then "Produse și servicii (creare)" window is opened
	And I input "HAMIE" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I click Select button of "Depozit" field
	Then "Depozite, Departamente" window is opened
	And I move one line up in "List" table
	And I select current line in "List" table
	Then "Produse și servicii (creare) *" window is opened
	And I move to the next attribute
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I go to line in "List" table
		| 'Short name' |
		| 'kg'         |
	And I select current line in "List" table
	And I click "Salvare şi închidere" button
	And I go to line in "List" table
		| 'Cod'         | 'Descriere' | 'UM' | 'Tip'      |
		| '00-00000007' | 'HAMIE'     | 'kg' | 'Inventar' |
	And I select current line in "List" table
	Then "HAMIE (Produse și servicii)" window is opened
	And I input "HAMEI" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I click the button named "FormCreateFolder"
	Then "Produse și servicii (creare grup)" window is opened
	And I input "Produse Finite" text in "Descriere" field
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'      |
		| '00-00000008' | 'Produse Finite' |
	#And I move one level up in "List" table
	And I move one level down in "List" table
	And I click the button named "FormCreate"
	Then "Produse și servicii (creare)" window is opened
	And I input "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 4.5%" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select "l" exact value from "Unitate de masură" drop-down list
	And I move to the next attribute
	And I click "Salvare" button
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I change checkbox "Caracteristici"
	And I change checkbox "Loturi"
	And I click "Salvare" button
	And In this window I click command interface button "Caracteristici"
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "EP" text in "Descriere" field
	And I move to the next attribute
	And I input "11.0" text in "Descriere" field
	And I click "Salvare" button
	And I input "12.72" text in "Descriere" field
	And I click "Salvare" button
	And I click "Salvare şi închidere" button
	Then "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "11.0" text in "Descriere" field
	And I click "Salvare şi închidere" button
	#Then "* (Caracteristică de produse și servicii)" window is opened
	#And I input "11.0" text in "Descriere" field
	#And I move to the next attribute
	#And I click "Salvare" button
	#And I click "Salvare şi închidere" button
	Then "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And In this window I click command interface button "Principală"
	And I click Select button of "Depozit" field
	Then "Depozite, Departamente" window is opened
	And I go to line in "List" table
		| 'Descriere'                | 'Tip'     | 'Slip' |
		| 'Depozitul Produse Finite' | 'Depozit' | 'Nu'   |
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I add a line in "List" table
	Then "Produse și servicii (creare)" window is opened
	And I input "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5%" text in "Nume scurt" field
	And I move to "Parametri principali" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And In this window I click command interface button "Caracteristici"
	Then "1C:Enterprise" window is opened
	And I click "OK" button
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I go to line in "List" table
		| 'Short name' |
		| 'l'          |
	And I select current line in "List" table
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I select current line in "List" table
	Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And In this window I click command interface button "Caracteristici"
	And I click the button named "FormCreate"
	And In this window I click command interface button "Principală"
	And I move to "Parametrii de stocare și cumpărare" tab
	And I change checkbox "Caracteristici"
	And I change checkbox "Loturi"
	And I click "Salvare" button
	And In this window I click command interface button "Caracteristici"
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "11.0" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "11.43" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'                                     | 'UM' | 'Tip'      |
		| '00-00000009' | 'CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 4.5%' | 'l'  | 'Inventar' |
	And I select current line in "List" table
	Then "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And In this window I click command interface button "Caracteristici"
	And I select current line in "List" table
	Then "11.0 (Caracteristică de produse și servicii)" window is opened
	And I input "12.5" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And In this window I click command interface button "Principală"
	And I input "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 5.0%" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'                                     | 'UM' | 'Tip'      |
		| '00-00000009' | 'CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 5.0%' | 'l'  | 'Inventar' |
	And I select current line in "List" table
	Then "CAROL BEER PILS NEFILTRATA, EP.11.0 ALC. 5.0% (Produse și servicii)" window is opened
	And I input "CAROL BEER PILS NEFILTRATA, EP.12.5 ALC. 5.0%" text in "Nume scurt" field
	#And I move to the next attribute
	#And I click "Salvare" button
	#And In this window I click command interface button "Caracteristici"
	#And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	#And I go to line in "List" table
	#	| 'Cod'         | 'Descriere'                                      | 'UM' | 'Tip'      |
	#	| '00-00000010' | 'CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5%' | 'l'  | 'Inventar' |
	#Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	#And In this window I click command interface button "Caracteristici"
	#And In this window I click command interface button "Principală"
	#And I click "Salvare şi închidere" button
	#Then "Produse și servicii" window is opened
	#And I add a line in "List" table
	#Then "Produse și servicii (creare)" window is opened
	#And I close "Produse și servicii (creare)" window
	#Then "Produse și servicii" window is opened
	#And I move one line down in "List" table
	And I select current line in "List" table
	And I click the button named "FormCopy"
	#And I add a line in "List" table
	Then "Produse și servicii (creare)" window is opened
	And I input "CAROL BEER WEIZEN NEFILTRATA, EP.12.5 ALC. 5.0%" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And In this window I click command interface button "Caracteristici"
	Then "1C:Enterprise" window is opened
	And I click "OK" button
	Then "CAROL BEER WEIZEN NEFILTRATA, EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "12.5" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "CAROL BEER WEIZEN NEFILTRATA, EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And I add a line in "List" table
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "12.4" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "CAROL BEER WEIZEN NEFILTRATA, EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I add a line in "List" table
	Then "Produse și servicii (creare)" window is opened
	And I input "CAROL BEER WEIZEN 0,5 L. EP.12.5 ALC. 5.0%" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Parametrii de stocare și cumpărare" tab
	And I move to "Pagini" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I select current line in "List" table
	And I move to "Parametrii de stocare și cumpărare" tab
	And I change checkbox "Caracteristici"
	And I change checkbox "Loturi"
	And I click "Salvare" button
	And In this window I click command interface button "Caracteristici"
	#Then "1C:Enterprise" window is opened
	#And I click "OK" button
	#Then "CAROL BEER WEIZEN * L. EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "12.5" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "CAROL BEER WEIZEN * L. EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And I click the button named "FormCreate"
	Then "Caracteristică de produse și servicii (creare)" window is opened
	And I input "12.4" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "CAROL BEER WEIZEN * L. EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And In this window I click command interface button "Principală"
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I go to line in "List" table
		| 'Short name' |
		| 'buc'        |
	And I select current line in "List" table
	Then "CAROL BEER WEIZEN * L. EP.12.5 ALC. 5.0% (Produse și servicii) *" window is opened
	And I move to the next attribute
	And I move to "Parametri principali" tab
	And I move to "Pagini" tab
	And I move to "Informații generale" tab
	And I move to "Pagini" tab
	And I input "00000093" text in "Articol" field
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "CAROL BEER WEIZEN NEFILTRATA, EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And I input "00000086" text in "Articol" field
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'                                      | 'UM' | 'Tip'      |
		| '00-00000010' | 'CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5%' | 'l'  | 'Inventar' |
	And I select current line in "List" table
	Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	#And I input "00" text in "Nume scurt" field
	And I input "00000063" text in "Articol" field
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'                                     | 'UM' | 'Tip'      |
		| '00-00000009' | 'CAROL BEER PILS NEFILTRATA, EP.12.5 ALC. 5.0%' | 'l'  | 'Inventar' |
	And I select current line in "List" table
	Then "CAROL BEER PILS NEFILTRATA, EP.12.5 ALC. 5.0% (Produse și servicii)" window is opened
	And I input "00000028" text in "Articol" field
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I move one line up in "List" table
	And I select current line in "List" table
	Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And In this window I click command interface button "Bills of materials"
	And I click the button named "FormCreate"
	Then "Factură de materiale (creare)" window is opened
	And I input "RETETAR001" text in "Descriere" field
	And I click Choice button of the field named "ProductCharacteristic"
	Then "Caracteristicile nomenclatorului" window is opened
	And I move one line down in "List" table
	And I move one line up in "List" table
	And I select current line in "List" table
	Then "Factură de materiale (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I input "11.00 RETETAR" text in "Descriere" field
	And I move to the next attribute
	And I click "Salvare" button
	And in the table "Content" I click the button named "ContentAdd"
	And I select "Material" exact value from "Line type" drop-down list in "Content" table
	And I move to the next attribute
	#And I click choice button of "Produse și servicii" attribute in "Content" table
	And I click choice button of "Produse și servicii" attribute in "Content" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "11.00 RETETAR (Factură de materiale) *" window is opened
	And I select "APA" exact value from "Produse și servicii" drop-down list in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I input "2,000" text in "Recepționat" field of "Content" table
	And I move to the next attribute
	And I select "l (unitate de stocare)" exact value from "UM" drop-down list in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I finish line editing in "Content" table
	And I add a line in "Content" table
	And I activate "Nr" field in "Content" table
	And I activate "Line type" field in "Content" table
	And I select "Material" exact value from "Line type" drop-down list in "Content" table
	And I move to the next attribute
	#And I click choice button of "Produse și servicii" attribute in "Content" table
	And I click choice button of "Produse și servicii" attribute in "Content" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "11.00 RETETAR (Factură de materiale) *" window is opened
	And I select "Seminte de grâu" exact value from "Produse și servicii" drop-down list in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	#And I click choice button of "Caracteristică" attribute in "Content" table
	#Then "Caracteristicile nomenclatorului" window is opened
	#And in the table "List" I click the button named "ListCreate"
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "CLASA 1" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I go to line in "List" table
	#	| 'Descriere' |
	#	| 'CLASA 1'   |
	#And I add a line in "List" table
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "CLASA 2" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I add a line in "List" table
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "CLASA 3" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I go to line in "List" table
	#	| 'Descriere' |
	#	| 'CLASA 1'   |
	#And I select current line in "List" table
	#Then "11.00 RETETAR (Factură de materiale) *" window is opened
	And I move to the next attribute
	And I input "0,100" text in "Recepționat" field of "Content" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from "UM" drop-down list in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I finish line editing in "Content" table
	And I add a line in "Content" table
	And I activate "Nr" field in "Content" table
	And I activate "Line type" field in "Content" table
	And I click choice button of "Line type" attribute in "Content" table
	#And I click choice button of "Line type" attribute in "Content" table
	And I finish line editing in "Content" table
	And I click choice button of "Line type" attribute in "Content" table
	And I select "Material" exact value from "Line type" drop-down list in "Content" table
	And I finish line editing in "Content" table
	And I move to the next attribute
	And I select current line in "Content" table
	#And I click choice button of "Produse și servicii" attribute in "Content" table
	And I click choice button of "Produse și servicii" attribute in "Content" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "11.00 RETETAR (Factură de materiale) *" window is opened
	And I select "DROJDII" exact value from "Produse și servicii" drop-down list in "Content" table
	And I finish line editing in "Content" table
	And I move to the next attribute
	And I select current line in "Content" table
	#And I click choice button of "Caracteristică" attribute in "Content" table
	#Then "Caracteristicile nomenclatorului" window is opened
	#And in the table "List" I click the button named "ListCreate"
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "SORT 1" text in "Descriere" field
	#And I move to the next attribute
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I move one line down in "List" table
	#And I add a line in "List" table
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "SORT 2" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I add a line in "List" table
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "SORT 3" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I move one line up in "List" table
	#And I move one line up in "List" table
	#And I move one line up in "List" table
	#And I select current line in "List" table
	#Then "11.00 RETETAR (Factură de materiale) *" window is opened
	#And I select "SORT 1" exact value from "Caracteristică" drop-down list in "Content" table
	#And I finish line editing in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "Content" table
	And I input "0,010" text in "Recepționat" field of "Content" table
	And I finish line editing in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I add a line in "Content" table
	And I activate "Line type" field in "Content" table
	And I select "Material" exact value from "Line type" drop-down list in "Content" table
	And I move to the next attribute
	And I click choice button of "Produse și servicii" attribute in "Content" table
	#And I click choice button of "Produse și servicii" attribute in "Content" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "11.00 RETETAR (Factură de materiale) *" window is opened
	And I select "HAMEI" exact value from "Produse și servicii" drop-down list in "Content" table
	And I move to the next attribute
	#And I click choice button of "Caracteristică" attribute in "Content" table
	#Then "Caracteristicile nomenclatorului" window is opened
	#And in the table "List" I click the button named "ListCreate"
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "1 SORT" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I move one line down in "List" table
	#And I add a line in "List" table
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "2 SORT" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	#And I add a line in "List" table
	#Then "Caracteristică de produse și servicii (creare)" window is opened
	#And I input "3 SORT" text in "Descriere" field
	#And I click "Salvare şi închidere" button
	#Then "Caracteristicile nomenclatorului" window is opened
	And I move one line up in "List" table
	And I move one line up in "List" table
	And I move one line up in "List" table
	#And in the table "List" I click the button named "ListChoose"
	Then "11.00 RETETAR (Factură de materiale) *" window is opened
	And I move to the next attribute
	And I input "0,050" text in "Recepționat" field of "Content" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from "UM" drop-down list in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I add a line in "Content" table
	And I activate "Nr" field in "Content" table
	And I activate "Line type" field in "Content" table
	And I select "Material" exact value from "Line type" drop-down list in "Content" table
	And I move to the next attribute
	And I click choice button of "Produse și servicii" attribute in "Content" table
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "11.00 RETETAR (Factură de materiale) *" window is opened
	And I select "HAMEI" exact value from "Produse și servicii" drop-down list in "Content" table
	#And I move to the next attribute
	#And I select "1 SORT" exact value from "Caracteristică" drop-down list in "Content" table
	And I move to the next attribute
	And I input "0,500" text in "Recepționat" field of "Content" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from "UM" drop-down list in "Content" table
	And I input "0,050" text in "Recepționat" field of "Content" table
	And I move to the next attribute
	And I select "kg (unitate de stocare)" exact value from "UM" drop-down list in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I finish line editing in "Content" table
	And I add a line in "Content" table
	And I activate "Nr" field in "Content" table
	And I activate "Line type" field in "Content" table
	And I click choice button of "Line type" attribute in "Content" table
	And I click "Salvare" button
	And I click "Salvare şi închidere" button
	Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And I click the button named "FormCopy"
	Then "Factură de materiale (creare)" window is opened
	And I click Choice button of the field named "ProductCharacteristic"
	Then "Caracteristicile nomenclatorului" window is opened
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Factură de materiale (creare) *" window is opened
	And I move to the next attribute
	And I input "11.43 RETETAR" text in "Descriere" field
	And I move to the next attribute
	And I move one line down in "Content" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "Content" table
	And I input "0,150" text in "Recepționat" field of "Content" table
	And I finish line editing in "Content" table
	And I input "0,110" text in "Recepționat" field of "Content" table
	And I finish line editing in "Content" table
	And I click "Salvare şi închidere" button
	Then "CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5% (Produse și servicii)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I go to line in "List" table
		| 'Cod'         | 'Descriere'                                  | 'UM'  | 'SKU'      | 'Tip'      |
		| '00-00000012' | 'CAROL BEER WEIZEN 0,5 L. EP.12.5 ALC. 5.0%' | 'buc' | '00000093' | 'Inventar' |
	And I close "Produse și servicii" window

