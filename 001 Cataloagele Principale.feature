﻿#language: en
#coding: utf-8

@tree

Feature: Indeplinirea cataloagelor principale

As Manager
I want Indeplinirea cataloagelor principale
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Indeplinirea cataloagelor principale

	When I click command interface button "Companie"
	When I click command interface button "Toate cataloagele"
	Then "De referință de informații" window is opened
	And I click "Valute" button
	Then "Valute" window is opened
	And I go to line in "Currencies" table
		| 'Nume valută' | 'Codul numeric' | 'Symbolic code' |
		| 'RON'         | '946'           | 'RON'           |
	And I select current line in "Currencies" table
	Then "RON (Valută)" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I input "946" text in "Codul numeric" field
	And I click "Salvare şi închidere" button
	Then "Valute" window is opened
	And I close "Valute" window

