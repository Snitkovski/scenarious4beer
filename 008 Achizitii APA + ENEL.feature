#language: en
#coding: utf-8

@tree

Feature: Achizitionarea apa si enel

As Manager
I want Achizitionarea apa si enel
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Achizitionarea apa si enel

	When I click command interface button "Achiziții"
	When I click command interface button "Cumpărări mărfuri și servicii"
	Then "Cumpărări mărfuri și servicii" window is opened
	#When I click command interface button "Produse și servicii"
	#When I click command interface button "Achiziții"
	#Then "Produse și servicii" window is opened
	#When I click command interface button "Cumpărări mărfuri și servicii"
	#Then "Cumpărări mărfuri și servicii" window is opened
	And I click the button named "FormCreate"
	Then "Factură furnizor (creare)" window is opened
	And I click Select button of "Furnizor" field
	Then "Furnizori" window is opened
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Factură furnizor (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I add a line in "Inventory" table
	And I activate field named "InventoryProductsAndServices" in "Inventory" table
	And I select "e" by string from the drop-down list named "InventoryProductsAndServices" in "Inventory" table
	And I move to the next attribute
	And I click choice button of "Caracteristică" attribute in "Inventory" table
	Then "Caracteristicile nomenclatorului" window is opened
	And I close "Caracteristicile nomenclatorului" window
	Then "Factură furnizor (creare) *" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I input "555,000" text in "Cantitatea" field of "Inventory" table
	And I move to the next attribute
	And I select "kW * h (unitate de stocare)" exact value from the drop-down list named "InventoryMeasurementUnit" in "Inventory" table
	And I move to the next attribute
	And I input "0,45" text in "Prețul" field of "Inventory" table
	And I finish line editing in "Inventory" table
	#And I add a line in "Inventory" table
	#And I activate field named "InventoryLineNumber" in "Inventory" table
	#And I activate field named "InventoryProductsAndServices" in "Inventory" table
	#And I click Select button of "datat" field
	And I input "14.06.2018  0:00:00" text in "datat" field
	And I click "Validare şi închidere" button
	Then "Cumpărări mărfuri și servicii" window is opened
	