#language: en
#coding: utf-8

@tree

Feature: Transfer si productie

As Manager
I want Transfer si productie
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Transfer si productie

	When in sections panel I select "Achiziții"
	When I click command interface button "Cumpărări mărfuri și servicii"
	Then "Cumpărări mărfuri și servicii" window is opened
	And I go to line in "List" table
		| 'Parteneri' | 'Date'                | 'Număr'       | 'Operațiunea'           | 'Sumă'        | 'Depozit'             |
		| 'APA CANAL' | '12.06.2018 12:00:00' | '0000-000002' | 'Receipt from supplier' | '390,00, RON' | 'Depozitul Principal' |
	And I click the button named "FormDocumentInventoryTransferCreateBasedOn"
	Then "Mișcarea inventarului (creare)" window is opened
	#And I click Select button of "Date" field
	#And I select from "Destinatar" drop-down list by "13.06.2018  0:00:00" string
	And I input "13.06.2018 00:00:00" text in "Date" field
	#And I move to the next attribute
	And I input "Depozitul Materia Prima" text in "Destinatar" field
	And I click "Validare şi închidere" button
	Then "Cumpărări mărfuri și servicii" window is opened
	And I close "Cumpărări mărfuri și servicii" window
	When I click command interface button "Achiziții"
	When I click command interface button "Inventory movements"
	Then "Inventory movements" window is opened
	And I click the button named "FormCreate"
	Then "Mișcarea inventarului (creare)" window is opened
	And I click Select button of "Destinatar" field
	Then "Depozite, Departamente" window is opened
	And I move one level down in "List" table
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "Mișcarea inventarului (creare) *" window is opened
	And in the table "Inventory" I click "By remaining goods in warehouse" button
	And I select "Depozitul Materia Prima" exact value from "De la" drop-down list
	And in the table "Inventory" I click "By remaining goods in warehouse" button
	Then "1C:Enterprise" window is opened
	And I click "Da" button
	Then "Mișcarea inventarului (creare) *" window is opened
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' |
		| '1'  | 'kg' | '10,000'     | 'DROJDII'             |
	And I delete a line in "Inventory" table
	And I move one line down in "Inventory" table
	And I move one line down in "Inventory" table
	And I move one line down in "Inventory" table
	And I add a line in "Inventory" table
	And I activate "Produse și servicii" field in "Inventory" table
	And I finish line editing in "Inventory" table
	And I activate "Nr" field in "Inventory" table
	And I delete a line in "Inventory" table
	And I delete a line in "Inventory" table
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' | 'Lot'        |
		| '1'  | 'kg' | '50,000'     | 'DROJDII'             | '04.06.2018' |
	And I activate "Cantitatea" field in "Inventory" table
	And I select current line in "Inventory" table
	And I input "12,500" text in "Cantitatea" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I go to line in "Inventory" table
		| 'Caracteristică' | 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' |
		| 'APA1'           | '2'  | 'l'  | '3.000,000'  | 'APA'                 |
	And I select current line in "Inventory" table
	And I input "1.000,000" text in "Cantitatea" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' | 'Lot'        |
		| '3'  | 'kg' | '1.000,000'  | 'Seminte de grâu'     | '04.06.2018' |
	And I select current line in "Inventory" table
	And I input "250,000" text in "Cantitatea" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I input "18.06.2018 00:00:00" text in "Date" field
	And I click "Validare şi închidere" button
	Then "Inventory movements" window is opened
	And I click the button named "FormCopy"
	Then "Mișcarea inventarului (creare)" window is opened
	And I select from "De la" drop-down list by "19.06.2018 00:00:00" string
	And I click Select button of "Destinatar" field
	Then "Depozite, Departamente" window is opened
	And I move one level up in "List" table
	And I move one level down in "List" table
	And I go to line in "List" table
		| 'Descriere'    | 'Tip'     | 'Slip' |
		| 'FermentVas02' | 'Depozit' | 'Nu'   |
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "Mișcarea inventarului (creare) *" window is opened
	And I delete a line in "Inventory" table
	And I delete a line in "Inventory" table
	And I delete a line in "Inventory" table
	And in the table "Inventory" I click the button named "InventoryAdd"
	And I select "HAMEI" exact value from "Produse și servicii" drop-down list in "Inventory" table
	And I move to the next attribute
	And I activate "Lot" field in "Inventory" table
	And I click choice button of "Lot" attribute in "Inventory" table
	Then "Loturi" window is opened
	And I select current line in "List" table
	Then "Mișcarea inventarului (creare) *" window is opened
	And I activate "Cantitatea" field in "Inventory" table
	And I input "50,000" text in "Cantitatea" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I click "Validare şi închidere" button
	Then "1C:Enterprise" window is opened
	And I click "OK" button
	Then "Mișcarea inventarului (creare) *" window is opened
	And in the table "Inventory" I click "By remaining goods in warehouse" button
	Then "1C:Enterprise" window is opened
	And I click "Da" button
	Then "Mișcarea inventarului (creare) *" window is opened
	And I click "Validare şi închidere" button
	Then "Inventory movements" window is opened
	And I go to line in "List" table
		| 'Tipul de operațiune' | 'Date'                | 'Destinatar:' | 'Număr'       | 'De la'                   |
		| 'Circulaţie'          | '18.06.2018 12:00:00' | 'BrasajVas01' | '0000-000003' | 'Depozitul Materia Prima' |
	And I activate "De la" field in "List" table
	And I click the button named "FormCopy"
	Then "Mișcarea inventarului (creare)" window is opened
	And I activate "Produse și servicii" field in "Inventory" table
	And I delete a line in "Inventory" table
	And I delete a line in "Inventory" table
	And I delete a line in "Inventory" table
	And I select "FermentVas02" exact value from "Destinatar" drop-down list
	And I input "19.06.2018 00:00:00" text in "Date" field
	And in the table "Inventory" I click the button named "InventoryAdd"
	And I select "HAMEI" exact value from "Produse și servicii" drop-down list in "Inventory" table
	And I move to the next attribute
	And I activate "Lot" field in "Inventory" table
	And I select "04.06.2018" exact value from "Lot" drop-down list in "Inventory" table
	And I move to the next attribute
	And I input "50,000" text in "Cantitatea" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I click "Validare şi închidere" button
	When in sections panel I select "Producție"
	Then "Inventory movements" window is opened
	When I click command interface button "Producție"
	Then "Producție" window is opened
	And I click the button named "FormCreate"
	Then "Producție (creare)" window is opened
	And I click Select button of "Producător" field
	Then "Depozite, Departamente" window is opened
	And I click the button named "FormCreate"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "Sala Productie" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "Depozite, Departamente" window is opened
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "Producție (creare) *" window is opened
	And in the table "Products" I click the button named "ProductsAdd"
	And I click choice button of "Produse și servicii" attribute in "Products" table
	Then "Produse și servicii" window is opened
	And I move one level down in "List" table
	And I select current line in "List" table
	Then "Producție (creare) *" window is opened
	And I activate field named "ProductsCharacteristic" in "Products" table
	And I select "11.43" exact value from the drop-down list named "ProductsCharacteristic" in "Products" table
	And I activate field named "ProductsQuantity" in "Products" table
	And I input "1.000,000" text in "Cantitatea" field of "Products" table
	And I activate field named "ProductsSpecification" in "Products" table
	And I click choice button of "Factură de materiale" attribute in "Products" table
	Then "Bills of materials" window is opened
	And I select current line in "List" table
	Then "Producție (creare) *" window is opened
	And I move to "Materiale" tab
	And I finish line editing in "Products" table
	And I move to "Pagini" tab
	And in the table "Inventory" I click "Fill in with BOM" button
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' |
		| '1'  | 'l'  | '2.000,000'  | 'APA'                 |
	And I activate field named "InventoryQuantity" in "Inventory" table
	And I select current line in "Inventory" table
	And I input "1.000,000" text in "Cantitatea" field of "Inventory" table
	And I activate field named "InventoryCharacteristic" in "Inventory" table
	And I select "APA1" exact value from the drop-down list named "InventoryCharacteristic" in "Inventory" table
	And I finish line editing in "Inventory" table
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' |
		| '4'  | 'kg' | '100,000'    | 'HAMEI'               |
	And I activate field named "InventoryQuantity" in "Inventory" table
	And I select current line in "Inventory" table
	And I input "50,000" text in "Cantitatea" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I input "20.06.2018 00:00:00" text in "datat" field
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' |
		| '2'  | 'kg' | '110,000'    | 'Seminte de grâu'     |
	And I activate field named "InventoryBatch" in "Inventory" table
	And I select current line in "Inventory" table
	And I click choice button of "Lot" attribute in "Inventory" table
	Then "Loturi" window is opened
	And I select current line in "List" table
	Then "Producție (creare) *" window is opened
	And I finish line editing in "Inventory" table
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' |
		| '3'  | 'kg' | '10,000'     | 'DROJDII'             |
	And I select current line in "Inventory" table
	And I click choice button of "Lot" attribute in "Inventory" table
	Then "Loturi" window is opened
	And I select current line in "List" table
	Then "Producție (creare) *" window is opened
	And I finish line editing in "Inventory" table
	And I go to line in "Inventory" table
		| 'Nr' | 'UM' | 'Cantitatea' | 'Produse și servicii' |
		| '4'  | 'kg' | '50,000'     | 'HAMEI'               |
	And I select current line in "Inventory" table
	And I click choice button of "Lot" attribute in "Inventory" table
	Then "Loturi" window is opened
	And I select current line in "List" table
	Then "Producție (creare) *" window is opened
	And I finish line editing in "Inventory" table
	And I click Choice button of the field named "InventoryStructuralUnitAssembling"
	Then "Depozite, Departamente" window is opened
	And I move one level down in "List" table
	And I go to line in "List" table
		| 'Descriere'    | 'Tip'     | 'Slip' |
		| 'FermentVas02' | 'Depozit' | 'Nu'   |
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "Producție (creare) *" window is opened
	And I click "Validare şi închidere" button
	Then "Producție" window is opened
	And I activate field named "ProductsAndServicesList" in "List" table
	And I click the button named "FormDocumentCostAllocationCreateBasedOn"
	Then "Repartizarea cheltuielilor (creare)" window is opened
	And I move to "Goods" tab
	And I move to "Pagini" tab
	And I move to "Principale" tab
	And I move to "Pagini" tab
	And I move to "Goods" tab
	And I move to "Pagini" tab
	And I move to "Inventar" tab
	And I move to "Pagini" tab
	And in the table "Inventory" I click the button named "InventoryAdd"
	And I select "Energie Electrica" exact value from the drop-down list named "InventoryProductsAndServices" in "Inventory" table
	And I move to the next attribute
	And I click choice button of "Caracteristică" attribute in "Inventory" table
	Then "Caracteristicile nomenclatorului" window is opened
	And I close "Caracteristicile nomenclatorului" window
	Then "Repartizarea cheltuielilor (creare) *" window is opened
	And I add a line in "Inventory" table
	And I activate field named "InventoryLineNumber" in "Inventory" table
	And I activate field named "InventoryProductsAndServices" in "Inventory" table
	And in the table "Inventory" I click the button named "InventoryPick"
	Then "Selectare" window is opened
	And I go to line in "InventoryList" table
		| 'Descriere'         | 'UM'     |
		| 'Energie Electrica' | 'kW * h' |
	And I select current line in "InventoryList" table
	And I click "Aplica" button
	Then "Repartizarea cheltuielilor (creare) *" window is opened
	And I go to line in "Inventory" table
		| 'Nr' | 'UM'     | 'Produse și servicii' | 'Recepționat' |
		| '1'  | 'kW * h' | 'Energie Electrica'   | '1,000'       |
	And I activate field named "InventoryQuantity" in "Inventory" table
	And I select current line in "Inventory" table
	And I input "100,000" text in "Recepționat" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And in the table "InventoryDistribution" I click the button named "InventoryDistributionAdd"
	And I input "1 000,000" text in "Recepționat" field of "InventoryDistribution" table
	And I move to the next attribute
	And I move to the next attribute
	And I select "ca" by string from the drop-down list named "InventoryDistributionProductsAndServices" in "InventoryDistribution" table
	And I move to the next attribute
	And I select "11.43" exact value from the drop-down list named "InventoryDistributionCharacteristic" in "InventoryDistribution" table
	And I move to the next attribute
	And I click choice button of "Lot" attribute in "InventoryDistribution" table
	And I move to the next attribute
	And I select "11.43 RETETAR" exact value from the drop-down list named "InventoryDistributionSpecification" in "InventoryDistribution" table
	And I move to the next attribute
	And I finish line editing in "InventoryDistribution" table
	And I add a line in "InventoryDistribution" table
	And I activate field named "InventoryDistributionLineNumber" in "InventoryDistribution" table
	And I activate field named "InventoryDistributionCount" in "InventoryDistribution" table
	And I move to "Goods" tab
	And I move to "Pagini" tab
	And I move to "Inventar" tab
	And I move to "Pagini" tab
	And I move to "Cheltuieli" tab
	And I move to "Pagini" tab
	And I move to "Inventar" tab
	And I move to "Pagini" tab
	And I activate field named "InventoryProductsAndServices" in "Inventory" table
	And I move to "Cheltuieli" tab
	And I move to "Pagini" tab
	And in the table "Costs" I click "Fill in by balance" button
	And I move to "Inventar" tab
	And I move to "Pagini" tab
	And I delete a line in "Inventory" table
	And I move to "Goods" tab
	And I move to "Pagini" tab
	And I move to "Cheltuieli" tab
	And I move to "Pagini" tab
	And in the table "Costs" I click "Fill in by balance" button
	And in the table "Costs" I click "Fill in by balance" button
	And in the table "Costs" I click "Fill in by balance" button
	And in the table "Costs" I click "Fill in by balance" button
	And I move to "Principale" tab
	And I move to "Pagini" tab
	And I select from "Departament" drop-down list by "a" string
	#And I click Select button of "De la" field
	And I input "18.06.2018" text in "De la" field
	And I move to "Goods" tab
	And I move to "Pagini" tab
	And I move to "Cheltuieli" tab
	And I move to "Pagini" tab
	And in the table "Costs" I click "Fill in by balance" button
	And in the table "Costs" I click "Fill in by balance" button
	And in the table "Costs" I click "Fill in by balance" button
	And in the table "Costs" I click "Fill in by balance" button
	And in the table "Costs" I click the button named "CostsAdd"
	And I input "100,00" text in "Sumă" field of "Costs" table
	And I move to the next attribute
	And I click choice button of "Cont de cost" attribute in "Costs" table
	Then "Cont" window is opened
	And I close "Cont" window
	Then "Repartizarea cheltuielilor (creare) *" window is opened
	And I finish line editing in "Costs" table
	And I close "Repartizarea cheltuielilor (creare) *" window
	Then "1C:Enterprise" window is opened
	And I click "Nu" button
	Then "Producție" window is opened
	And I close "Producție" window
	Then "Inventory movements" window is opened
	And I close "Inventory movements" window


