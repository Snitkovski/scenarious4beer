#language: en
#coding: utf-8

@tree

Feature: Setari de valută si altele

As Manager
I want Setari de valută si altele
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Setari de valută si altele

	When I click command interface button "Companie"
	When I click command interface button "Toate cataloagele"
	Then "De referință de informații" window is opened
	And I click "Forme juridice" button
	Then "Forme juridice" window is opened
	And I select current line in "List" table
	If window with "Limited Liability Company (Forma juridică)" header has appeared Then
		Then "Limited Liability Company (Forma juridică)" window is opened
		And I input "SRL" text in "Short name" field
		And I input "Societate cu Raspundere Limitata" text in "Descriere" field
		And I click "Salvare şi închidere" button
		And I wait "Limited Liability Company (Forma juridică)" window closing in 20 seconds
		Then "Forme juridice" window is opened
	Else
		And I close "*" window
	#EndIF
	And I close "Forme juridice" window
	Then "De referință de informații" window is opened
	And I click "Valute" button
	Then "Valute" window is opened
	And I select current line in "Currencies" table
	Then "EUR (Valută)" window is opened
	And In this window I click command interface button "Exchange rates"
	And I activate "Valută" field in "List" table
	And I select current line in "List" table
	Then "Rata de schimb" window is opened
	And I input "4,6695" text in "Rata de schimb" field
	And I click "Salvare şi închidere" button
	And I wait "Rata de schimb" window closing in 20 seconds
	Then "EUR (Valută)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	And I wait "EUR (Valută)" window closing in 20 seconds
	Then "Valute" window is opened
	And I go to line in "Currencies" table
		| 'Nume valută' | 'Codul numeric' | 'Symbolic code' |
		| 'US Dollar'   | '840'           | 'USD'           |
	And I select current line in "Currencies" table
	Then "USD (Valută)" window is opened
	And In this window I click command interface button "Exchange rates"
	And I activate "Valută" field in "List" table
	And I select current line in "List" table
	Then "Rata de schimb" window is opened
	And I input "4,0518" text in "Rata de schimb" field
	And I click "Salvare şi închidere" button
	And I wait "Rata de schimb" window closing in 20 seconds
	Then "USD (Valută)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	And I wait "USD (Valută)" window closing in 20 seconds
	Then "Valute" window is opened
	And I click the button named "FormCreate"
	Then "1C:Enterprise" window is opened
	And I click "Creare" button
	Then "Valută (creare)" window is opened
	And I input "RON" text in "Nume valută" field
	And I input "946" text in "Codul numeric" field
	And I change "Rata de schimb" radio button value to "imported from the Internet"
	And I click "Salvare şi închidere" button
	And I wait "Valută (creare)" window closing in 20 seconds
	#And I input "" text in "Codul numeric" field
	#And I input "946" text in "Codul simbolic" field
	#And I click "Salvare şi închidere" button
	#And I wait "Valută (creare)" window closing in 20 seconds
	And I input "946" text in "Codul numeric" field
	And I input "RON" text in "Codul simbolic" field
	And I click "Salvare şi închidere" button
	And I wait "Valută (creare)" window closing in 20 seconds
	Then "Valute" window is opened
	And I close "Valute" window
	Then "De referință de informații" window is opened
	And I click "Cote TVA" button
	Then "Cote TVA" window is opened
	And I select current line in "List" table
	Then "15% (Cota TVA)" window is opened
	And I input "19%" text in "Descriere" field
	And I input "19,00" text in "Rata" field
	And I click "Salvare şi închidere" button
	And I wait "15% (Cota TVA)" window closing in 20 seconds
	Then "Cote TVA" window is opened
	And in the table "List" I click the button named "ListContextMenuCopy"
	Then "Cota TVA (creare)" window is opened
	And I input "9%" text in "Descriere" field
	And I input "9,00" text in "Rata" field
	And I click "Salvare şi închidere" button
	And I wait "Cota TVA (creare)" window closing in 20 seconds
	Then "Cote TVA" window is opened
	And I click the button named "FormCreate"
	Then "Cota TVA (creare)" window is opened
	And I close "Cota TVA (creare)" window
	Then "Cote TVA" window is opened
	And I go to line in "List" table
		| 'Descriere'       | 'De calcul' |
		| 'Exempt from VAT' | 'Nu'        |
	And I select current line in "List" table
	Then "Exempt from VAT (Cota TVA)" window is opened
	And I input "Taxare inversa" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Exempt from VAT (Cota TVA)" window closing in 20 seconds
	Then "Cote TVA" window is opened
	And I close "Cote TVA" window
	Then "De referință de informații" window is opened
	And I click "Tax kinds" button
	Then "Tax kinds" window is opened
	And I select current line in "List" table
	Then "Income tax (Impozit)" window is opened
	And I input "Impozitul pe Venit" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Income tax (Impozit)" window closing in 20 seconds
	Then "Tax kinds" window is opened
	And I go to line in "List" table
		| 'Cod'       | 'Descriere' | 'Account for return'             | 'Contul GL'                         |
		| '000000001' | 'VAT'       | 'Current taxes receivable - VAT' | 'Other current taxes payable - VAT' |
	And I select current line in "List" table
	Then "VAT (Impozit)" window is opened
	And I input "TVA" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "VAT (Impozit)" window closing in 20 seconds
	Then "Tax kinds" window is opened
	And I close "Tax kinds" window
	Then "De referință de informații" window is opened
	And I click "calendare" button
	Then "calendare" window is opened
	And I select current line in "List" table
	Then "Five days calendar (Orarul lucrărilor)" window is opened
	And I activate "Prezentare zi" field in "FillTemplate" table
	And I input "Calendarul din Romania" text in "Denumire" field
	And I click "Salvare şi închidere" button
	And I wait "Five days calendar (Orarul lucrărilor)" window closing in 20 seconds
	Then "calendare" window is opened
	And I close "calendare" window
	Then "De referință de informații" window is opened
	And I click "Țările lumii" button
	Then "Țările lumii" window is opened
	And in the table "List" I click the button named "ListCreate"
	Then "1C:Enterprise" window is opened
	And I click "Da" button
	Then "World CountriesClassifier" window is opened
	And in the table "Classifier" I click the button named "FindClassifier"
	Then "Găsește" window is opened
	And I input "moldova" text in "&Ce căutăm" field
	And I click "&Găsește" button
	Then "World CountriesClassifier" window is opened
	And in the table "Classifier" I click the button named "ClassifierChoose"
	Then "Țările lumii" window is opened
	And in the table "List" I click the button named "ListCreate"
	Then "1C:Enterprise" window is opened
	And I click "Da" button
	Then "World CountriesClassifier" window is opened
	And in the table "Classifier" I click the button named "FindClassifier"
	Then "Găsește" window is opened
	And I input "romania" text in "&Ce căutăm" field
	And I click "&Găsește" button
	Then "World CountriesClassifier" window is opened
	And I select current line in "Classifier" table
	Then "Țările lumii" window is opened
	And in the table "List" I click the button named "ListCreate"
	Then "1C:Enterprise" window is opened
	And I click "Da" button
	Then "World CountriesClassifier" window is opened
	And I go to line in "Classifier" table
		| 'Alfa code 3' | 'Cod' | 'Descriere' | 'Denumirea completă' | 'Alfa code 2' |
		| 'HUN'         | '348' | 'HUNGARY'   | 'Hungary'            | 'HU'          |
	And I select current line in "Classifier" table
	Then "Țările lumii" window is opened
	And I close "Țările lumii" window
	Then "De referință de informații" window is opened
	And I click "Depozite" hyperlink
	Then "Depozite" window is opened
	And I select current line in "List" table
	Then "Main warehouse (Unitate de afaceri a unei companii)" window is opened
	And I input "Depozitul Principal" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Main warehouse (Unitate de afaceri a unei companii)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "Depozitul Materia Prima" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "Depozitul Produse Finite" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "Vase de Fermentatie" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "FermentVas01" text in "Descriere" field
	And I click Select button of "Grup" field
	Then "Depozite, Departamente" window is opened
	And I go to line in "List" table
		| 'Descriere'           | 'Tip'     | 'Slip' |
		| 'Vase de Fermentatie' | 'Depozit' | 'Nu'   |
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "Unitate de afaceri a unei companii (creare) *" window is opened
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare) *" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "FermentVas02" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "FermentVas03" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I move one level up in "List" table
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "Vase de Brasaj" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "BrasajVas01" text in "Descriere" field
	And I select from "Grup" drop-down list by "Vase de Brasaj" string
	And I click Select button of "Grup" field
	Then "Depozite, Departamente" window is opened
	And I go to line in "List" table
		| 'Descriere'      | 'Tip'     | 'Slip' |
		| 'Vase de Brasaj' | 'Depozit' | 'Nu'   |
	And I activate "Descriere" field in "List" table
	And I select current line in "List" table
	Then "Unitate de afaceri a unei companii (creare) *" window is opened
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare) *" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "BrasajVas02" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "BrasajVas03" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I move one level up in "List" table
	And I click the button named "FormCreate"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "Departament Vinzari" text in "Descriere" field
	And I select from "Tip" drop-down list by "Departament" string
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Depozite" window is opened
	And I go to line in "List" table
		| 'Descriere'           | 'Tip'     | 'Slip' |
		| 'Depozitul Principal' | 'Depozit' | 'Nu'   |
	And I click "Use as the Main warehouse" button
	And I close "Depozite" window
	Then "De referință de informații" window is opened
	And I click "Departamente" hyperlink
	Then "Departamente" window is opened
	And I go to line in "List" table
		| 'Descriere'       | 'Tip'         |
		| 'Main department' | 'Departament' |
	And I select current line in "List" table
	Then "Main department (Unitate de afaceri a unei companii)" window is opened
	And I input "Administratia" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Main department (Unitate de afaceri a unei companii)" window closing in 20 seconds
	Then "Departamente" window is opened
	And I click the button named "FormCopy"
	Then "Unitate de afaceri a unei companii (creare)" window is opened
	And I input "Departament Achizitii" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Unitate de afaceri a unei companii (creare)" window closing in 20 seconds
	Then "Departamente" window is opened
	And I close "Departamente" window
	Then "De referință de informații" window is opened
	And I click "Genul de activitate" hyperlink
	Then "Main activity (Domeniul de afaceri)" window is opened
	And I input "Producere Brasaj" text in "Descriere" field
	And I click "Salvare şi închidere" button
	And I wait "Main activity (Domeniul de afaceri)" window closing in 20 seconds
	Then "De referință de informații" window is opened
	And I click "Grupuri de produse şi servicii" button
	Then "Grupuri de produse şi servicii" window is opened
	And I close "Grupuri de produse şi servicii" window
	Then "De referință de informații" window is opened
	And I click "Clasificatorul unităților de măsură" button
	Then "Clasificatorul unităților de măsură" window is opened
	And I go to line in "List" table
		| 'Nume deplin' | 'Short name' |
		| 'Pieces'      | 'pcs'        |
	And I activate "Nume deplin" field in "List" table
	And I select current line in "List" table
	Then "pcs (Unitate de masură)" window is opened
	And I input "buc" text in "Short name" field
	And I move to the next attribute
	And I input "Bucati" text in "Nume deplin" field
	And I click "Salvare şi închidere" button
	And I wait "pcs (Unitate de masură)" window closing in 20 seconds
	Then "Clasificatorul unităților de măsură" window is opened
	And I close "Clasificatorul unităților de măsură" window
	Then "De referință de informații" window is opened
	And I click "Contract forms" button
	Then "Contract forms" window is opened
	And I select current line in "List" table
	Then "Purchase and Sale Contract (Contract forms)" window is opened
	And I input "Contract de Vinzare - Cumparare" text in "Denumire" field
	And I click "Salvare şi închidere" button
	And I wait "Purchase and Sale Contract (Contract forms)" window closing in 20 seconds
	Then "Contract forms" window is opened
	And I close "Contract forms" window
	When I click command interface button "Vânzări"
	When I click command interface button "Tipurile prețurilor"
	Then "Tipul prețurilor de vânzare" window is opened
	And I select current line in "List" table
	Then "Accounting price (Tipul prețurilor nomenclatorului)" window is opened
	And I input "Pret Client Final " text in "Descriere" field
	And I click Select button of "Valuta prețului" field
	Then "Valute" window is opened
	And I go to line in "Currencies" table
		| 'Nume valută' | 'Codul numeric' | 'Symbolic code' |
		| 'RON'         | '946'           | 'RON'           |
	And I select current line in "Currencies" table
	Then "Accounting price (Tipul prețurilor nomenclatorului) *" window is opened
	And I click "Salvare şi închidere" button
	And I wait "Accounting price (Tipul prețurilor nomenclatorului) *" window closing in 20 seconds
	Then "Tipul prețurilor de vânzare" window is opened
	And I go to line in "List" table
		| 'Descriere'            | 'Incl. TVA' |
		| 'Wholesale price, EUR' | 'Nu'        |
	And I select current line in "List" table
	Then "Wholesale price (Tipul prețurilor nomenclatorului)" window is opened
	And I input "Pret Angro" text in "Descriere" field
	And I select from "Valuta prețului" drop-down list by "RON" string
	And I click "Salvare şi închidere" button
	And I wait "Wholesale price (Tipul prețurilor nomenclatorului)" window closing in 20 seconds
	Then "Tipul prețurilor de vânzare" window is opened
	And I close "Tipul prețurilor de vânzare" window
	Then "De referință de informații" window is opened
	And I close "De referință de informații" window

