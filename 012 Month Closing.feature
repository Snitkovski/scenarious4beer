#language: en
#coding: utf-8

@tree

Feature: Procedura inchiderea lunii

As Manager
I want Procedura inchiderea lunii
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Procedura inchiderea lunii

	When in sections panel I select "Companie"
	When I click command interface button "Month-end closing"
	Then "Month-end closing" window is opened
	And I move to "Iunie" tab
	And I change checkbox "Accrue depreciation"
	And I change checkbox "Calculate direct costs"
	And I change checkbox "Allocate costs"
	And I change checkbox "Calculare costul de preț efectiv"
	And I change checkbox "Calculate exchange rate differences"
	And I change checkbox "Calculate financial result"
	And I click "Month-end closing" button
	And I click "Raport" button
	Then "Month-end closing report" window is opened
	

