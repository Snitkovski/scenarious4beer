#language: en
#coding: utf-8

@tree

Feature: Indeplinirea catalogului Parteneri

As Manager
I want Indeplinirea catalogului Parteneri
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Indeplinirea catalogului Parteneri

	When I click command interface button "Vânzări"
	When I click command interface button "Clienți"
	Then "Clienți" window is opened
	And I click the button named "Create"
	Then "Partener (creare)" window is opened
	And I input "Client 001" text in "Nume legal" field
	And I move to the next attribute
	And I move to the next attribute
	And I select "Societate de Raspundere Limitata" exact value from "Legal form" drop-down list
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I input "RO567890" text in "CUI" field
	And I move to the next attribute
	And I move to the next attribute
	And I input "J27/654/2011" text in "Numărul de înregistrare de stat" field
	And I move to the next attribute
	And I select from "Registration country" drop-down list by "ro" string
	And I move to the next attribute
	And I click "Salvare" button
	And In this window I click command interface button "Contracte"
	And I select current line in "List" table
	#Then "Contractul implicit (Contract)" window is opened
	And I input "1312" text in "Nr." field
	And I move to the next attribute
	#And I click Select button of "Date" field
	And I input "15.01.2018" text in "Date" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click Select button of "Contract type" field
	Then "Counterparty contract types" window is opened
	And I close "Counterparty contract types" window
	Then "Contractul implicit (Contract) *" window is opened
	And I move to "Decontări" tab
	And I move to "Pages" tab
	And I click "Salvare şi închidere" button
	Then "Client * (Cumpărător)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Clienți" window is opened
	And I add a line in "List" table
	Then "Partener (creare)" window is opened
	And I input "Client 002" text in "Nume legal" field
	And I move to the next attribute
	And I input "Client 002" text in "Alias" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I move to "Detalii de contact" tab
	And I move to "GroupPages" tab
	And I move to "Domeniul de aplicare al activităților" tab
	And I move to "GroupPages" tab
	And I move to "General" tab
	And I move to "GroupPages" tab
	And In this window I click command interface button "Contracte"
	Then "1C:Enterprise" window is opened
	And I click "OK" button
	And I select current line in "List" table
	Then "Contractul implicit (Contract)" window is opened
	And I move to "Decontări" tab
	And I move to "Pages" tab
	And I click Select button of "Tip de preț" field
	Then "Tipul prețurilor de vânzare" window is opened
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Contractul implicit (Contract) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Client 002 (Cumpărător)" window is opened
	And I click "Setați ca implicit" button
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Clienți" window is opened
	And I change checkbox "Furnizori"
	And I click the button named "Create"
	Then "Partener (creare)" window is opened
	And I input "Furnizor Materii Prime 001" text in "Nume legal" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select "Societate cu Raspundere Limitata" exact value from "Legal form" drop-down list
	And I move to the next attribute
	And I click "Salvare" button
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I input "RO" text in "CUI" field
	And I move to the next attribute
	And I input "" text in "VAT number" field
	And I input "RO35431" text in "CUI" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select from "Registration country" drop-down list by "ro" string
	And I move to the next attribute
	And I move to "Detalii de contact" tab
	And I move to "GroupPages" tab
	And I move to "Domeniul de aplicare al activităților" tab
	And I move to "GroupPages" tab
	And I move to "Detalii de contact" tab
	And I move to "GroupPages" tab
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I move to "General" tab
	And I move to "GroupPages" tab
	And I click "Salvare" button
	And In this window I click command interface button "Contracte"
	And I select current line in "List" table
	Then "Contractul implicit (Contract)" window is opened
	And I move to "Decontări" tab
	And I move to "Pages" tab
	And I click Select button of "Tipul de preț al partenerului" field
	Then "Tipurile prețurilor nomenclatorului furnizorilor" window is opened
	And I click the button named "FormCreate"
	Then "Tipul prețului nomenclatorului furnizorului (creare)" window is opened
	And I move to the next attribute
	And I move to the next attribute
	And I input "Pret Furnizor 001" text in "Descriere" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select "Nu" exact value from "Prețul include TVA" drop-down list
	And I click "Salvare şi închidere" button
	Then "Tipurile prețurilor nomenclatorului furnizorilor" window is opened
	And I select current line in "List" table
	Then "Contractul implicit (Contract) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Furnizor Materii Prime * (Cumpărător, Furnizor)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Clienți, Furnizori" window is opened
	And I add a line in "List" table
	Then "Partener (creare)" window is opened
	And I input "Furnizor Materii Prime 002" text in "Nume legal" field
	And I move to the next attribute
	And I input "Furnizor Materii Prime 002" text in "Alias" field
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I input "RO5656767" text in "CUI" field
	And I move to the next attribute
	And I click "Salvare" button
	And In this window I click command interface button "Contracte"
	And I select current line in "List" table
	Then "Contractul implicit (Contract)" window is opened
	And I move to "Decontări" tab
	And I move to "Pages" tab
	And I click Select button of "Tip de preț" field
	Then "Tipul prețurilor de vânzare" window is opened
	And I close "Tipul prețurilor de vânzare" window
	Then "Contractul implicit (Contract)" window is opened
	And I click Select button of "Tipul de preț al partenerului" field
	Then "Tipurile prețurilor nomenclatorului furnizorilor" window is opened
	And I click the button named "FormCreate"
	Then "Tipul prețului nomenclatorului furnizorului (creare)" window is opened
	And I input "Pret Furnizor 002" text in "Descriere" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select "Nu" exact value from "Prețul include TVA" drop-down list
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Tipurile prețurilor nomenclatorului furnizorilor" window is opened
	And I select current line in "List" table
	Then "Contractul implicit (Contract) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Furnizor Materii Prime * (Cumpărător, Furnizor)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	Then "Clienți, Furnizori" window is opened
	And I go to line in "List" table
		| 'Sumarul șirului de căutare' | 'Prezentare' | 'Nume legal' |
		| 'Client 002\nAdministrator'  | 'Client 002' | 'Client 002' |
	And I select current line in "List" table
	Then "Client * (Cumpărător)" window is opened
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I input "RO34567" text in "CUI" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Clienți, Furnizori" window is opened
	And I click "Creare" button
	Then "Partener (creare)" window is opened
	And I input "Furnizor ENEL" text in "Nume legal" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select "Societate cu Raspundere Limitata" exact value from "Legal form" drop-down list
	And I move to the next attribute
	And I click "Salvare" button
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I input "RO8765456" text in "CUI" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select from "Registration country" drop-down list by "r" string
	And I move to the next attribute
	And I move to "Detalii de contact" tab
	And I move to "GroupPages" tab
	And I move to "Decontări și coduri" tab
	And I move to "GroupPages" tab
	And I click "Salvare" button
	And In this window I click command interface button "Contracte"
	And I select current line in "List" table
	Then "Contractul implicit (Contract)" window is opened
	And I move to "Decontări" tab
	And I move to "Pages" tab
	And I click Select button of "Tipul de preț al partenerului" field
	Then "Tipurile prețurilor nomenclatorului furnizorilor" window is opened
	And I click the button named "FormCreate"
	Then "Tipul prețului nomenclatorului furnizorului (creare)" window is opened
	And I input "Pret Furnizor ENEL" text in "Descriere" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select "Nu" exact value from "Prețul include TVA" drop-down list
	And I click "Salvare şi închidere" button
	Then "Tipurile prețurilor nomenclatorului furnizorilor" window is opened
	And I select current line in "List" table
	Then "Contractul implicit (Contract) *" window is opened
	And I click "Salvare şi închidere" button
	Then "Furnizor ENEL (Cumpărător, Furnizor)" window is opened
	And In this window I click command interface button "Principală"
	And I click "Salvare şi închidere" button
	And I close "Clienți, Furnizori" window

