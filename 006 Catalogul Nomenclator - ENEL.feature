#language: en
#coding: utf-8

@tree

Feature: Indeplinirea catalogului Nomenclator si ENEL

As Manager
I want Indeplinirea catalogului Nomenclator si ENEL
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Indeplinirea catalogului Nomenclator si ENEL

	When I click command interface button "Vânzări"
	When I click command interface button "Produse și servicii"
	Then "Produse și servicii" window is opened
	And I click the button named "FormCreateFolder"
	Then "Produse și servicii (creare grup)" window is opened
	And I input "Suplimentar" text in "Descriere" field
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I move one line down in "List" table and select line
	And I move one line up in "List" table
	And I move one level down in "List" table
	And I click the button named "FormCreateFolder"
	Then "Produse și servicii (creare grup)" window is opened
	And I close "Produse și servicii (creare grup)" window
	Then "Produse și servicii" window is opened
	And I click the button named "FormCreate"
	Then "Produse și servicii (creare)" window is opened
	And I input "Energie Electrica" text in "Nume scurt" field
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click Select button of "Unitate de masură" field
	Then "Clasificatorul unităților de măsură" window is opened
	And I click the button named "FormCreate"
	Then "Unitate de masură (creare)" window is opened
	And I close "Unitate de masură (creare)" window
	Then "Clasificatorul unităților de măsură" window is opened
	And I click the button named "FormCreate"
	Then "Unitate de masură (creare)" window is opened
	And I input "kW * h" text in "Short name" field
	And I click "Salvare şi închidere" button
	Then "Clasificatorul unităților de măsură" window is opened
	And I select current line in "List" table
	Then "Produse și servicii (creare) *" window is opened
	And I move to the next attribute
	And I click "Salvare şi închidere" button
	Then "Produse și servicii" window is opened
	And I move one level up in "List" table
	And I close "Produse și servicii" window

