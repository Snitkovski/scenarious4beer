#language: en
#coding: utf-8

@tree

Feature: Vanzare

As Manager
I want Vanzare
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Vanzare

	When I click command interface button "Vânzări"
	When I click command interface button "Facturi de vânzare"
	Then "Facturi de vânzare" window is opened
	And I click the button named "FormCreate"
	Then "Facturi de vânzare (creare)" window is opened
	#And I click Select button of "Date" field
	And I input "21.06.2018  0:00:00" text in "Date" field
	And I select from "Cumpărător" drop-down list by "cl" string
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click Select button of "Depozit" field
	Then "Depozite" window is opened
	And I close "Depozite" window
	Then "Facturi de vânzare (creare) *" window is opened
	And I input "Depozitul Produse Finite" text in "Depozit" field
	#When I click command interface button "Achiziții"
	#Then "Facturi de vânzare (creare) *" window is opened
	#When I click command interface button "Rapoarte suplimentare"
	#Then "Rapoarte suplimentare (Secțiunea \"Purchases\")" window is opened
	#And I click "Revocare" button
	#When I click command interface button "Achiziții"
	#Then "Facturi de vânzare (creare) *" window is opened
	#When I click command interface button "Purchasing and warehouse reports"
	#Then "Inventarul și achizițiile" window is opened
	#And I close "Inventarul și achizițiile" window
	#When I click command interface button "Achiziții"
	#Then "Facturi de vânzare (creare) *" window is opened
	When in sections panel I select "Producție"
	When I click command interface button "Producție"
	Then "Producție" window is opened
	And I activate field named "StructuralUnit" in "List" table
	And I select current line in "List" table
	Then "Producție * de la *" window is opened
	And I close "Producție * de la *" window
	Then "Producție" window is opened
	And I input "Depozitul Produse Finite" text in "Destinatar" field 
	And I click "Validare şi închidere" button
	Then "Producție" window is opened
	And I close "Producție" window
	Then "Facturi de vânzare (creare) *" window is opened
	#And I input "Sala Productie" text in "Depozit" field
	#And I click Select button of "Depozit" field
	#Then "Depozite" window is opened
	#And I move one line up in "List" table
	#And I move one line down in "List" table and select line
	#And I move one line up in "List" table
	#And I move one level down in "List" table
	#And I move one level up in "List" table
	#And I move one line down in "List" table
	#And I move one level down in "List" table
	#And I move one level up in "List" table
	#And I move one line down in "List" table
	#And I move one level down in "List" table
	#And I move one level up in "List" table
	#And I move one line down in "List" table
	#And I move one level down in "List" table
	#And I move one level up in "List" table
	#And I move one line down in "List" table
	#And I move one level down in "List" table
	#And I move one level up in "List" table
	#And I move one level down in "List" table
	#And I move one level up in "List" table
	#And I close "Depozite" window
	#When I click command interface button "Producție"
	Then "Facturi de vânzare (creare) *" window is opened
	#When I click command interface button "Producție"
	#Then "Producție" window is opened
	#And I activate field named "StructuralUnit" in "List" table
	#And I select current line in "List" table
	#Then "Producție * de la *" window is opened
	#And I click Choice button of the field named "ProductsStructuralUnitAssembling"
	#Then "Depozite, Departamente" window is opened
	#And I go to line in "List" table
	#	| 'Descriere'                | 'Tip'     | 'Slip' |
	#	| 'Depozitul Produse Finite' | 'Depozit' | 'Nu'   |
	#And I activate "Descriere" field in "List" table
	#And I select current line in "List" table
	#Then "Producție * de la * *" window is opened
	#And I click "Validare şi închidere" button
	#Then "Producție" window is opened
	#And I close "Producție" window
	Then "Facturi de vânzare (creare) *" window is opened
	And I input "Depozitul Produse Finite" text in "Depozit" field
	#And I click Select button of "Depozit" field
	#Then "Depozite" window is opened
	#And I move one line down in "List" table
	#And I select current line in "List" table
	#Then "Facturi de vânzare (creare) *" window is opened
	And I move to the next attribute
	#And I click Select button of "Depozit" field
	#Then "Depozite" window is opened
	#And I activate "Descriere" field in "List" table
	#And I select current line in "List" table
	#Then "Facturi de vânzare (creare) *" window is opened
	And in the table "Inventory" I click the button named "InventoryPick"
	Then "Select products and services in sales documents" window is opened
	And I go to line in "ListInventory" table
		| 'Descriere'                                      | 'UM' | 'Unrestricted stock' |
		| 'CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5%' | 'l'  | '1 000,0'            |
	And I select current line in "ListInventory" table
	And I move to "Caracteristicile pagina" tab
	And I move to "PagesProductsAndServicesCharacteristics" tab
	Then "Sfat" window is opened
	And I click "OK" button
	Then "Select products and services in sales documents" window is opened
	And I go to line in "CharacteristicsList" table
		| 'Characteristics: <...>' | 'Unrestricted stock' | 'UM' |
		| '11.43'                  | '1 000,0'            | 'l'  |
	And I select current line in "CharacteristicsList" table
	And I activate "Prețul" field in "CartPriceBalanceReserveCharacteristic" table
	And I select current line in "CartPriceBalanceReserveCharacteristic" table
	And I input "4 375,00" text in "Prețul" field of "CartPriceBalanceReserveCharacteristic" table
	And I finish line editing in "CartPriceBalanceReserveCharacteristic" table
	And I select current line in "CartPriceBalanceReserveCharacteristic" table
	And I input "4,75" text in "Prețul" field of "CartPriceBalanceReserveCharacteristic" table
	And I finish line editing in "CartPriceBalanceReserveCharacteristic" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I select current line in "CartPriceBalanceReserveCharacteristic" table
	And I input "300,000" text in "Recepționat" field of "CartPriceBalanceReserveCharacteristic" table
	And I finish line editing in "CartPriceBalanceReserveCharacteristic" table
	And I click "Move document" button
	Then "Facturi de vânzare (creare) *" window is opened
	And I activate "Prețul" field in "Inventory" table
	#And I select current line in "Inventory" table
	And I go to line in "Inventory" table
		| 'Caracteristică' | 'Produs sau serviciu'                            | 'Nr' | 'Prețul' | 'Cantitatea' | 'Rezervă' | 'Unitate' | 'Sumă'     | 'VAT %' | 'TVA'    | 'Total'    |
		| '11.43'          | 'CAROL BEER LAGER NEFILTRATA, EP.11.0 ALC. 4.5%' | '1'  | '4,75'   | '300,000'    | '2,000'   | 'l'       | '1.425,00' | '19%'   | '270,75' | '1.695,75' |
	And I input "6,38" text in "Prețul" field of "Inventory" table
	And I finish line editing in "Inventory" table
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I move to the next attribute
	And I click "Validare şi închidere" button

