#language: en
#coding: utf-8

@tree

Feature: Transfer intre gestiuni (daca are cazul)

As Manager
I want Transfer intre gestiuni
To Demonstrarea clientului

Background:
	Given I launch TestClient opening script or connect the existing one


Scenario: Transfer intre gestiuni

	When I click command interface button "Vânzări"
	When I click command interface button "Achiziții"
	When I click command interface button "Cumpărări mărfuri și servicii"
	Then "Cumpărări mărfuri și servicii" window is opened
	And I go to line in "List" table
		| 'Parteneri'                  | 'Date'                | 'Număr'       | 'Operațiunea'           | 'Sumă'           | 'Depozit'             |
		| 'Furnizor Materii Prime 001' | '04.06.2018 12:00:00' | '0000-000001' | 'Receipt from supplier' | '7.126,91, RON'  | 'Depozitul Principal' |
	And I activate "Depozit" field in "List" table
	And I activate "Parteneri" field in "List" table
	And I click the button named "FormDocumentInventoryTransferCreateBasedOn"
	Then "Mișcarea inventarului (creare)" window is opened
	And I move to the next attribute
	And I click Select button of "Destinatar" field
	Then "Depozite, Departamente" window is opened
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I move one line down in "List" table
	And I select current line in "List" table
	Then "Mișcarea inventarului (creare) *" window is opened
	And I move to the next attribute
	#And I click Select button of "Date" field
	#And I input "20.06.2018  0:00:00" text in "Date" field
	#And I click Select button of "Date" field
	And I input "05.06.2018  0:00:00" text in "Date" field
	And I click "Validare şi închidere" button

